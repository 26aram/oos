﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AddressService.DataModel
{
    public class AddressRespondeModel
    {
        public result[] results { get; set; }
        public string status { get; set; }
    }


    public class result
    {
        public address_component[] address_components { get; set; }

        public string formatted_address { get; set; }
        public string place_id { get; set; }
        public string[] types { get; set; }

        public geometry geometry { get; set; }

    }
    public class geometry
    {
        public bound bounds { get; set; }
        public latlng location { get; set; }
        public string location_type { get; set; }

        public bound viewport { get; set; }
    }

    public class bound
    {
        public latlng northeast { get; set; }
        public latlng southwest { get; set; }
    }

    public class latlng
    {
        public decimal lat { get; set; }
        public decimal lng { get; set; }
       
    }


    public class address_component
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }
}