﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AddressService.BusinessModel
{
   


    public class BaseManager : IDisposable
    {
        public const string googleAPIkey = "AIzaSyCuAVuPtBtlxRUg6Zty2BOB6Qrz6N-G4A0";
        public const string UspsUserID = "622GAGA07378";


        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void LogException(System.Exception exp)
        {

            logger.Trace(exp.StackTrace);
            //logger.Debug("Sample debug message, k={0}, l={1}", k, l);
            //logger.Info("Sample informational message, k={0}, l={1}", k, l);
            //logger.Warn("Sample warning message, k={0}, l={1}", k, l);
            logger.Error(exp.Message);
            //logger.Fatal("Sample fatal error message, k={0}, l={1}", k, l);
            //logger.Log(LogLevel.Info, "Sample informational message, k={0}, l={1}", k, l);
        }




        public BaseManager()
        {

        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
            }
            // free native resources if there are any.
        }

    }
}