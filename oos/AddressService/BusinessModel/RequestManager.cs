﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace AddressService.BusinessModel
{
    public class RequestManager: BaseManager
    {
        public string Get(string url)
        {
            string response = null;
            using (var wb = new WebClient())
            {
                response = wb.DownloadString(url);
            }
            return response;
        }



        public string Post(string url, NameValueCollection data)
        {
            string response = null;
            using (var wb = new WebClient())
            {
                //var data = new NameValueCollection();
                //data["username"] = "myUser";
                //data["password"] = "myPassword";

                var responseArray = wb.UploadValues(url, "POST", data);
                response = Encoding.UTF8.GetString(responseArray);
            }

            return response;
        }
    }
}