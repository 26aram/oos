﻿<!DOCTYPE html>
<html> 
<head> 
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/> 
  <title>Google Maps API Geocoding Demo</title> 
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyCuAVuPtBtlxRUg6Zty2BOB6Qrz6N-G4A0" type="text/javascript"></script>
</head> 
<body>
  <div id="map" style="width: 500px; height: 400px;"></div>

  <script type="text/javascript">
    var address1 = 'London, UK';
    var address2 = 'New York, US';

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: new google.maps.LatLng(35.00, -25.00),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var gc = new google.maps.Geocoder();

    gc.geocode({'address': address1}, function (res1, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            new google.maps.Marker({
                position: res1[0].geometry.location,
                map: map
            });

        gc.geocode({'address': address2}, function (res2, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            new google.maps.Marker({
              position: res1[0].geometry.location,
              map: map
            });

            new google.maps.Marker({
              position: res2[0].geometry.location,
              map: map
            });

            new google.maps.Polyline({
              path: [
                res1[0].geometry.location,
                res2[0].geometry.location
              ],
              strokeColor: '#FF0000',
              geodesic: true,
              map: map
            });
          }
        });
      }            
    });

  </script>
</body>
</html>