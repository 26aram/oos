﻿using AddressService.BusinessModel;
using AddressService.DataModel;
using Facad;

using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;

namespace AddressService
{
    //https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class AddressService : IAddressService
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

       
        public CompositeType VerifyAddress(AddressModel composite)
        {

            if (string.IsNullOrEmpty(composite.Address1) || string.IsNullOrEmpty(composite.City) || string.IsNullOrEmpty(composite.State))
                return new CompositeType { BoolValue = false, StringValue = "Invalid Address" };
               
            using (var manager = new RequestManager())
            {
              var response =  manager.Get(string.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0}, {1} {2}, {3}&key={4}", composite.Address1, composite.Address2, composite.City, composite.State, BaseManager.googleAPIkey));

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();

                var rObj = json_serializer.Deserialize<AddressRespondeModel>(response);
            }

            return new CompositeType { BoolValue = true };
        }

        public CompositeType VerifyUspsAddress(AddressModel composite)
        {

            if (string.IsNullOrEmpty(composite.Address1) || string.IsNullOrEmpty(composite.City) || string.IsNullOrEmpty(composite.State))
                return new CompositeType { BoolValue = false, StringValue = "Invalid Address" };
               
            using (var manager = new USPSManager(BaseManager.UspsUserID, false))
            {



          
                USPSAddressModel a = new USPSAddressModel();
                a.Address1 = composite.Address1;
                a.Address2 = composite.Address2;
                a.City = composite.City;
                a.State = composite.State;
                // USPSAddressModel addressWithZip = manager.GetZipcode(a);


                try
                {
                    USPSAddressModel validatedAddress = manager.ValidateAddress(a);
                }
                catch(System.Exception exp)
                {
                    BaseManager.LogException(exp);
                    return new CompositeType { BoolValue = false, StringValue = exp.Message };
                }
             



              

            }

            return new CompositeType { BoolValue = true };
        }
    }
}
