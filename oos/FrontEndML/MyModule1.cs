﻿using FrontEndML.Controler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FrontEndML
{
    public class MyModule1 : IHttpModule
    {
        /// <summary>
        /// You will need to configure this module in the Web.config file of your
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpModule Members

        public void Dispose()
        {
            //clean-up code here.
        }

        public void Init(HttpApplication context)
        {
            // Below is an example of how you can handle LogRequest event and provide 
            // custom logging implementation for it
            context.LogRequest += new EventHandler(OnLogRequest);

            context.BeginRequest += Context_BeginRequest;
            context.EndRequest += Context_EndRequest;
            context.Error += Context_Error;

            //SetTimer();
        }



        private void Context_EndRequest(object sender, EventArgs e)
        {

        }

        public void OnLogRequest(Object source, EventArgs e)
        {
            //custom logging logic can go here
        }

        #endregion



        private void Context_BeginRequest(object sender, EventArgs e)
        {
           
            BaseControler controler = UrlParser.Parse().Result;



            if (controler != null)
            {
                // to load any data befor render layout (master page)
                controler.PreloadData();

                if (controler.GetType() != typeof(BlobController))
                {
                    // renderbegining of layout (master page)
                    controler.BeginRender();

                    // render main content (CENTER)
                    controler.ContentRender();

                    // render end of layout (master page)
                    controler.EndRender();
                }
            }
            
        }

        private void Context_Error(object sender, EventArgs e)
        {

        }




    }
}
