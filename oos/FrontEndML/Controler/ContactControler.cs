﻿using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace FrontEndML.Controler
{
    public class ContactControler : BaseControler
    {

        public ContactControler(Client c) : base(c)
        {

        }

      

        public override void ContentRender()
        {
            base.ContentRender();
            sb.Append("<ul>");
            sb.Append("<li>");

            sb.Append("<div class=\"timeline-badge primary\"></div>");
            sb.Append("<div class=\"timeline-panel\">");
            sb.Append("<h2>Contact Us</h2>");
            sb.Append("<div class=\"hr-left\"></div>");
            sb.Append("<p> We appreciate your feedback. </p>");


            #region collect message

           
            var request = HttpContext.Current.Request;

            string text = request["text"];
            string email = request["email"];
            string subject = request["subject"];



            if (!string.IsNullOrEmpty(text))
            {

                try
                {


                    StringBuilder sbuilder = new StringBuilder();

                    sbuilder.AppendLine("Hi, You got the folowing feedback from user. See below:");

                    sbuilder.AppendLine("<table>");
                    sbuilder.AppendLine("<tr>");

                    sbuilder.AppendLine("<td>");
                    sbuilder.AppendLine("email:");
                    sbuilder.AppendLine("</td>");

                    sbuilder.AppendLine("<td>");
                    sbuilder.AppendLine(email);
                    sbuilder.AppendLine("</td>");

                    sbuilder.AppendLine("</tr>");
                    sbuilder.AppendLine("<tr>");


                    sbuilder.AppendLine("<td>");
                    sbuilder.AppendLine("subject:");
                    sbuilder.AppendLine("</td>");

                    sbuilder.AppendLine("<td>");
                    sbuilder.AppendLine(subject);
                    sbuilder.AppendLine("</td>");

                    sbuilder.AppendLine("</tr>");
                    sbuilder.AppendLine("<tr>");


                    sbuilder.AppendLine("<td>");
                    sbuilder.AppendLine("text:");
                    sbuilder.AppendLine("</td>");

                    sbuilder.AppendLine("<td>");
                    sbuilder.AppendLine(text);
                    sbuilder.AppendLine("</td>");

                    sbuilder.AppendLine("</tr>");

                    /*
                    sbuilder.AppendLine("<tr>");


                    sbuilder.AppendLine("<td>");
                    sbuilder.AppendLine("Referer:");
                    sbuilder.AppendLine("</td>");

                    sbuilder.AppendLine("<td>");
                    sbuilder.AppendLine("500Wine.com");
                    sbuilder.AppendLine("</td>");



                    sbuilder.AppendLine("</tr>");
                    */
                    sbuilder.AppendLine("</table>");
                    if (Send(sbuilder, "Feedback From Macazon User") == "OK")
                    {
                        sb.Append("<p class=\"thankyou\"> Thank Your For Feedback. </p>");
                    }

                }
                catch (System.Exception exp)
                {

                }
            }

            #endregion


            sb.Append("<form method=\"post\" action=\"\">");
            sb.Append("<div class=\"row\">");
            sb.Append("<div class=\"col-md-6\">");

            sb.Append("<div class=\"form-group\">");
            sb.Append("<input class=\"form-control input-lg\" required=\"\"  name=\"email\" placeholder=\"E-mail...\">");
            sb.Append("</div>");
            sb.Append("<div class=\"form-group\">");
            sb.Append("<input class=\"form-control input-lg\" required=\"\"  name=\"subject\" placeholder=\"Subject...\">");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<div class=\"col-md-6\">");
            sb.Append("<div class=\"form-group\">");
            sb.Append("<textarea class=\"form-control input-lg\" required=\"\" rows=\"7\" name=\"text\" placeholder=\"Messages...\"></textarea>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<div class=\"form-group\">");
            sb.Append("<button class=\"btn btn-lg btn-primary btn-block\">SEND</button>");
            sb.Append("</div>");
            sb.Append("</form>");
            sb.Append("</div>");
            sb.Append("</li>");
            sb.Append("</ul>");


        }



        const string fromPassword = "26Mailinator";
        const string fromAddress = "macazon.com@gmail.com";
        private string Send(StringBuilder sbuilder, string subj)
        {


            MailMessage EmailMsg = new MailMessage();
            EmailMsg.From = new MailAddress("macazon.com@gmail.com", "macazon.com User Feedback");
            EmailMsg.To.Add(new MailAddress(this.Client.CompanyEmail, this.Client.Company));
            EmailMsg.Subject = subj;
            EmailMsg.Body = sbuilder.ToString();
            EmailMsg.IsBodyHtml = true;
            EmailMsg.Priority = MailPriority.Normal;

            string msg = "OK";
            try
            {


                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress, fromPassword)
                };

                smtp.Send(EmailMsg);

            }
            catch (Exception exp)
            {

                return exp.Message;
            }

            return msg;
        }
    }
}