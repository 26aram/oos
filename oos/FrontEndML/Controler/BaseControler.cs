﻿using Facad.DataContract;
using FrontEndML.Controler.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FrontEndML.Controler
{
    public class BaseControler
    {
        public const string domainParam = "domainid";
        public const int PaginationItemCount = 5;
        public BaseControler(Client c):this()
        {
            this.client = c;
        }

        private Client client;
        public Client Client
        {
            get
            {
                return client;
            }
        }

        public string SerarchFormeActionPath { get; set; }
        public int caseID { get; set; }

        public int currentPage { get; set; }

        public string searchvalue { get; set; }

        public string title { get; set; }
        public string h1 { get; set; }

        protected StringBuilder sb { get; set; }
        public BaseControler()
        {
            this.sb = new StringBuilder();
        }

        public virtual void BeginRender()
        {

            sb.Append("<!DOCTYPE html>");
            Header.RenderFullHeader(sb, this.title, this.client);
            sb.AppendFormat("<body style=\"background-color:{0};\">", this.client.BackgroundColor);



            //<--gridcontainer
            sb.Append("<div class='gridcontainer'>");
            //<--gridwrapper
            sb.Append("<div class='gridwrapper'>");

            #region Header
            //<--Header
           
            sb.AppendFormat("<div class='gridbox gridheader' style=\"background-color:{0};\">", this.client.HeaderColor);
            SearchForm.RenderSearchForm(sb, this.searchvalue, this.h1, this.client);
            sb.Append("</div>");
            //<Header-->
            #endregion

            #region Horizontal Menu
            HorizontalMenu.Render(sb, h1, this.client);
            #endregion


            #region Left Site Bar
            sb.AppendFormat("<div class='gridbox gridmenu'  style=\"background-color:{0};\">", this.client.HeaderColor);
            sb.Append("<div class='left'>");
            MainMenu.RenderMenu(sb, this.client);
            sb.Append("</div>");

            sb.Append("</br>");
            sb.Append("<div class='left1'>");


            Reklam.RenderAds(sb);



            //LastSearchedList.RenderLastSerachedList(sb);
            sb.Append("</div>");
            sb.Append("</div>");
            #endregion


            //<--gridbox gridmain
            sb.Append("<div class='gridbox gridmain'>");
            //<--main
            sb.Append("<div class='main'>");




        }

        public virtual void EndRender()
        {





            //main-->
            sb.Append("</div>");
            //<gridbox gridmain-->
            sb.Append("</div>");


            #region Righr Site Bar
            sb.Append("<div class='gridbox gridright'>");
            sb.Append("<div class='right'>");

             Reklam.RenderAds(sb);
           
            sb.Append("</div>");
            sb.Append("</div>");
            #endregion


            #region Footer
            sb.Append("<div class='gridbox gridfooter'>");
            sb.AppendFormat("<div class='footer' style=\"background-color:{0};\">", this.client.HeaderColor);
            Footer.RenderFooter(sb);
            sb.Append("</div>");
            sb.Append("</div>");
            #endregion


            //gridwrapper-->
            sb.Append("</div>");
            //gridcontainer-->
            sb.Append("</div>");


            sb.Append("</body></html>");


            #region Respose End
            var response = HttpContext.Current.Response;
            response.ClearContent();
            response.BufferOutput = true;
            response.Write(sb);
            response.Flush();
            response.End();
            #endregion
        }


        public virtual void PreloadData()
        {

        }

        public virtual void ContentRender()
        {

        }








    }
}
