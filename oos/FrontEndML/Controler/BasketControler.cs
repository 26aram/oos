﻿using Facad.DataContract;
using FrontEndML.Controler.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FrontEndML.Controler
{
    public class BasketControler : BaseControler
    {

        public BasketControler(Client c):base(c)
        {
          
        }

        public override void ContentRender()
        {
            base.ContentRender();

            var bll = Provider.FrontEndBll;
            var tests = Converter.ReadCookie(null);
            if(tests!=null && tests.Count>0)
            {
                var shipIdList = tests.Select(s => s.s).ToList();
                var selectedShipmentList = bll.GetShipmentsSelectedByUser(shipIdList).Result;



                if (selectedShipmentList != null)
                {
                    sb.Append("<form role=\"form\" action=\"\" enctype=\"multipart/form-data\" method=\"post\">");

                    foreach (var item in selectedShipmentList)
                    {
                        int count = 0;
                        var testItem = tests.FirstOrDefault(f => f.s == item.ShipmentID);
                        if (testItem != null)
                            count = testItem.q;
                        ShipmentItemInBasket.RenderShipmentItem(sb, item, count);
                    }

                    if(selectedShipmentList.Count>0)
                    {
                        sb.Append("<input type=\"submit\" class=\"btn btn-lg btn-primary btn-block\" value=\"Proceed To Checkout\"></input>");
                    }

                    sb.Append("</form>");
                }



            }
        }
    }
}