﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FrontEndML.Controler
{
    public class BlobController : BaseControler
    {
        // GET: Blob

        public override void PreloadData()
        {
           var result = Provider.FrontEndBll.GetBlobById(caseID);

            var context = HttpContext.Current;
            var response = context.Response;
            if (result.Result!=null)
                response.BinaryWrite(result.Result);
            else
                response.BinaryWrite(new byte[0]);

            response.ContentType = "image/jpeg";

            response.Flush();
           
            response.End();


        }
       
    }
}