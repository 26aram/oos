﻿using Facad.DataContract;
using FrontEndBLL;
using FrontEndML.Controler.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndML.Controler
{
    public class HomeControler : BaseControler
    {
        int filterID = 0;
        string filter="/";
        public HomeControler(Client c, string group, int groupId):base(c)
        {
            this.filter = group;
            this.filterID = groupId;
        }

        public override void BeginRender()
        {

            var client = this.Client;

            var bll = Provider.FrontEndBll;


            string companyName = string.Empty;
            if (client.IncludeCompanyName)
                companyName = client.Company;
            this.title = this.h1 = string.Format("{0} {1}", companyName, searchvalue);

            SerarchFormeActionPath = "/";

            base.BeginRender();
            var shipments = new List<Facad.DataContract.Shipment>();
            int clientID = client.ClientID;
            if (clientID>0)
                shipments = bll.GetShipmentsForClient(clientID, this.searchvalue, this.currentPage, this.filterID).Result;
           
            if (shipments != null)
            {
               


                foreach (var item in shipments)
                {
                    ShipmentItem.RenderSipmentItem(sb, item);
                }

                sb.Append("<div class=\"paging\">");

                if (client.IsTest)
                {
                    if (this.currentPage > 0)
                    {
                        sb.AppendFormat("<a href=\"/?p={0}&{1}={2}{3}{4}\" class=\"kButton kLeft\"></a>", (this.currentPage - 1).ToString(), BaseControler.domainParam, clientID, 
                            string.IsNullOrEmpty(this.searchvalue)?"":"&q=" + this.searchvalue
                            ,this.filterID==0?"":"&g="+this.filterID);
                    }
                    if (shipments.Count == BaseControler.PaginationItemCount)
                    {
                        sb.AppendFormat("<a href=\"/?p={0}&{1}={2}{3}{4}\" class=\"kButton kRight\"></a>", (this.currentPage + 1).ToString(), BaseControler.domainParam, clientID
                            , string.IsNullOrEmpty(this.searchvalue) ? "" : "&q=" + this.searchvalue
                            , this.filterID == 0 ? "" : "&g=" + this.filterID);
                    }
                }
                else
                {
                    if (this.currentPage > 0)
                    {
                        sb.AppendFormat("<a href=\"/?p={0}{3}{4}\" class=\"kButton kLeft\"></a>", (this.currentPage - 1).ToString(), BaseControler.domainParam
                            , clientID, string.IsNullOrEmpty(this.searchvalue) ? "" : "&q=" + this.searchvalue
                            , this.filterID == 0 ? "" : "&g=" + this.filterID);
                    }
                    if (shipments.Count == BaseControler.PaginationItemCount)
                    {
                        sb.AppendFormat("<a href=\"/?p={0}{3}{4}\" class=\"kButton kRight\"></a>", (this.currentPage + 1).ToString(), BaseControler.domainParam
                            , clientID, string.IsNullOrEmpty(this.searchvalue) ? "" : "&q=" + this.searchvalue
                            , this.filterID == 0 ? "" : "&g=" + this.filterID);
                    }
                }

                sb.Append("</div>");
            }

        }

        public override void ContentRender()
        {
            base.ContentRender();



        }
    }
}
