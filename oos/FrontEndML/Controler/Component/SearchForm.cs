﻿using Facad.DataContract;
using FrontEndBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndML.Controler.Component
{
    public class SearchForm
    {
        public static void RenderSearchForm(StringBuilder sb, string value, string h1, Client c)
        {
            //<--Left
            sb.Append("<div class='gridbox gridmenu'>");

            sb.AppendFormat("<img class=\"logo\" alt=\"Logo\" src=\"/blob/{0}\">", c.LogoImageID.ToString());

            sb.Append("</div>");

            //Left-->

            //<--Middle
            sb.Append("<div class='gridbox gridmain'>");
            if (c.IncludeCompanyEmail)
                sb.AppendFormat("<h1>{0}</h1>", h1);


            if(c.IsTest)
                sb.AppendFormat("<form class=\"form-wrapper cf\" method=\"post\" action=\"/?{1}={0}\">", c.ClientID.ToString(), ClientManager.domainParam);
            else
                sb.AppendFormat("<form class=\"form-wrapper cf\" method=\"get\" action=\"/\">", c.ClientID.ToString());


            sb.Append("<table class=\"srchTbl\"><tr>");
            sb.Append("<td class=\"srchTdl\">");
            sb.AppendFormat("<input type=\"text\" name=\"q\" placeholder=\"Search here...\" required=\"\" value=\"{0}\">", value);
            sb.Append("</td>");
            sb.Append("<td class=\"srchTdr\">");
            sb.Append("<button type=\"submit\">&#128269;</button>");
            sb.Append("</td></tr>");
            sb.Append("</tr></table>");
            sb.Append("</form>");

            sb.Append(c.DeliveryNote);

            sb.Append("</div>");
            //Middle-->

            //<--Right
            sb.AppendFormat("<div class='gridbox gridright'>");
            if (c.IncludeCompanyEmail)
            {
                sb.AppendFormat("<h4>{0}</h4>", c.CompanyPhone);
            }
            if (c.IncludeCompanyEmail)
            {
                sb.AppendFormat("<h4>{0}</h4>", c.CompanyEmail);
            }
            sb.Append("</div>");
            //Right-->

          
           

        }
    }
}
