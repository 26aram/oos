﻿using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndML.Controler.Component
{
    public class MainMenu
    {
        public static void RenderMenu(StringBuilder sb, Client client)
        {





            var groups = client.Groups;
            if (groups != null)
            {
              
                if (client.IsTest)
                {
                    sb.AppendFormat("<div class='menuitem'><a href=\"/?{0}={1}\">All</a></div>", BaseControler.domainParam.ToString(), client.ClientID.ToString());


                    foreach (var item in groups)
                    {
                        sb.AppendFormat("<div class='menuitem'><a href=\"/{0}/{1}?{2}={3}\">{4}</a></div>",Converter.CleanAll(item.Value), item.Key.ToString(), BaseControler.domainParam.ToString(), client.ClientID.ToString(), item.Value);
                    }
                }
                else
                {
                    sb.Append("<div class='menuitem'><a href=\"/\">All</a></div>");

                    foreach (var item in groups)
                    {
                        sb.AppendFormat("<div class='menuitem'><a href=\"/{0}/{1}\">{2}</a></div>", Converter.CleanAll(item.Value), item.Key.ToString(),item.Value);
                    }
                }
            }
        

        }
    }
}
