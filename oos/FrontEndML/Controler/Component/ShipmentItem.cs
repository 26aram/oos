﻿using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace FrontEndML.Controler.Component
{
    public class ShipmentItem
    {
        public static void RenderSipmentItem(StringBuilder sb, Shipment sipment)
        {

            sb.Append("<div class=\"box\">");
            sb.AppendFormat("<h2 class=\"dColor\"><a href=\"#\">{0}</a></h2>", sipment.Name);
            sb.AppendFormat("<img class='fa' alt=\"state\" src=\"/blob/{0}\">", sipment.ImageID.ToString());
            sb.Append("<div class=\"text-box\">");



            sb.AppendFormat("<h3><span>Description:</span>{0}</h3>", sipment.Description);

            sb.AppendFormat("<h3><span>Unit Of Mesure:</span>{0}</h3>", sipment.UofM);
            sb.AppendFormat("<h3><span>Price:</span>${0}</h3>", sipment.Price);

            sb.AppendFormat("<h3><span>Count:</span><INPUT class='isNumber' maxlength='6' type='text' onkeypress='return isNumber(event)'><a id=\"{0}\" href='#' onclick='add(this)'>Add To Basket</a></h3>", sipment.ShipmentID.ToString());

            sb.Append("</div>");
            sb.Append("</div>");

        }


        private static string NameIdToLink(string name, int id)
        {
            return string.Format("{0}-{1}", Converter.CleanAll(name), id.ToString());
        }
    }
}