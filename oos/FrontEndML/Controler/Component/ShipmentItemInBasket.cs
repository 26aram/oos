﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Facad.DataContract;

namespace FrontEndML.Controler.Component
{
    public class ShipmentItemInBasket
    {

        public static void RenderShipmentItem(StringBuilder sb, Shipment shipment, int count)
        {

            sb.Append("<div class=\"box\">");
            sb.AppendFormat("<h2 class=\"dColor\">{0}</h2>", shipment.Name);
            sb.AppendFormat("<img class='fa' alt=\"Shipment Item\" src=\"/blob/{0}\">", shipment.ImageID.ToString());
            sb.Append("<div class=\"text-box\">");

            sb.AppendFormat("<h3><span>Description:</span>{0}</h3>", shipment.Description);

            sb.AppendFormat("<h3><span>Unit Of Mesure:</span>{0}</h3>", shipment.UofM);
            sb.AppendFormat("<h3><span>Price:</span>${0}</h3>", shipment.Price);

            sb.AppendFormat("<h3><span>Count:</span><INPUT class='isNumber' maxlength='6' type='text' onkeypress='return isNumber(event)' onchange='countTotal(this)' value ={1}><span>Total</span><label class=\"total\">${2}</label>", shipment.ShipmentID.ToString(), count.ToString(), (count* shipment.Price).ToString());
            sb.AppendFormat("<input type=\"hidden\" class=\"price\" name=\"price\" value=\"{0}\">", shipment.Price);
            sb.AppendFormat("<input type=\"hidden\" class=\"shipment\" name=\"shipment\" value=\"{0}\">", shipment.ShipmentID.ToString());
            sb.Append("</h3>");

            sb.AppendFormat("<a id=\"{0}\" href='#' onclick='deleteItem(this)'>Delete</a>", shipment.ShipmentID.ToString());
            sb.Append("</div>");
            sb.Append("</div>");

        }
    }
}