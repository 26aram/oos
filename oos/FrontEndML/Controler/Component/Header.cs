﻿using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndML.Controler.Component
{
    public class Header
    {
        public static void RenderFullHeader(StringBuilder sb, string title, Client c)
        {
            sb.Append("<html lang=\"en\">");
            sb.Append("<head>");
            sb.AppendFormat("<title>{0}</title>", title);
            sb.Append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            sb.Append("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">");
            sb.Append("<meta name=\"viewport\" content=\"width=device-width,initial-scale=1\">");
            sb.AppendFormat("<meta name=\"description\" content=\"Search the Best products and services to delivery your home {0}.\">", title);

            sb.Append("<script src=\"/scripts/main.js\"></script>");

            sb.Append("<link href=\"/css/main.css?0\" rel =\"stylesheet\"/>");
            sb.Append("<link rel=\"shortcut icon\" href=\"/favicon.ico\"/>");

            var hColor = c.HeaderColor;
            var bColor = c.BackgroundColor;

            if (!string.IsNullOrEmpty(hColor) && !string.IsNullOrEmpty(bColor))
            {

                sb.Append("<style>");
                sb.Append(".dColor{");
                sb.AppendLine();
                sb.AppendFormat("background: -moz-linear-gradient(873deg, {0} 0%, {1} 100%);",hColor, bColor);
                sb.AppendLine();
                sb.AppendFormat("background: -webkit-gradient(linear, right top, right bottom, color-stop(0%, {0}), color-stop(100%, {1}));", hColor, bColor);
                sb.AppendLine();
                sb.AppendFormat("background: -webkit-linear-gradient(873deg, {0} 0%, {1} 100%);", hColor, bColor);
                sb.AppendLine();
                sb.AppendFormat("background: -o-linear-gradient(873deg, {0} 0%, {1} 100%);", hColor, bColor);
                sb.AppendLine();
                sb.AppendFormat("background: -ms-linear-gradient(873deg, {0} 0%, {1} 100%);", hColor, bColor);
                sb.AppendLine();
                sb.AppendFormat("background: linear-gradient(873deg, {0} 0%, {1} 100%);", hColor, bColor);
                sb.AppendLine();
                sb.AppendFormat("filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='{0}', endColorstr='{1}',GradientType=0 );", hColor, bColor);
                sb.AppendLine();
                sb.Append("}");
                sb.Append("</style>");
            }

            sb.Append("</head>");
        }
    }
}
