﻿using Facad.DataContract;
using FrontEndBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndML.Controler.Component
{
    public class HorizontalMenu
    {
        public static void Render(StringBuilder sb, string title, Client c)
        {

            int itemCount = 0;
            var tests = Converter.ReadCookie(null);
            if (tests != null)
                itemCount = tests.Count;


            if (c.IsTest)
            {
                sb.Append("<ul id=\"menu\" class=\"nav dColor\">");
                sb.AppendFormat("<li><a href=\"\\?{1}={0}\">Home</a></li>", c.ClientID.ToString(), BaseControler.domainParam);
                sb.AppendFormat("<li><a href=\"/basket?{2}={0}\">Basket<div title=\"Items Count\" id=\"bskt\">{1}</div></a></li>", c.ClientID.ToString(), itemCount.ToString(), BaseControler.domainParam);
                sb.AppendFormat("<li><a href=\"/contact?{1}={0}\">Contact As</a></li>", c.ClientID.ToString(), BaseControler.domainParam);
                sb.Append("</ul>");
            }
            else
            {
                sb.Append("<ul id=\"menu\" class=\"nav dColor\">");
                sb.Append("<li><a href=\"/\">Home</a></li>");
                sb.AppendFormat("<li><a href=\"/basket\">Basket<div title=\"Items Count\" id=\"bskt\">{0}</div></a></li>", itemCount.ToString());
                sb.Append("<li><a href=\"/contact\">Contact As</a></li>");
                sb.Append("</ul>");
            }

        }
    }
}
