﻿using FrontEndML.Controler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FrontEndML
{
    public class UrlParser
    {
        public static async Task<BaseControler> Parse()
        {
            var request = HttpContext.Current.Request;

            BaseControler controler = null;


            string searchValue = request.QueryString["q"];
            if (string.IsNullOrEmpty(searchValue))
                searchValue = request["q"];
            string currentPage = request.QueryString["p"];
            int pageNumber = int.TryParse(currentPage, out pageNumber) ? pageNumber : 0;


            string path = request.Url.LocalPath;
            if (path.Contains(".asmx"))
                return null;
            else if (path.Contains(".html"))
                return null;
            else if (path.Contains(".xml"))
                return null;
            else if (path.Contains(".txt"))
                return null;

            var arr = path.Split(new[] { '/', '-' }, StringSplitOptions.RemoveEmptyEntries);
            int id = 0;
            if (arr.Length > 0)
            {
                path = arr[0].ToLower();
                int.TryParse(arr[arr.Length - 1], out id);
            }

            
            switch (path)
            {
                case "scripts":
                case "css":
                case "/css/main.css":
                case "/Services/WebService1.asmx":
                case "img":
                case "favicon.ico":
                    return null;

                    case "blob":
                        controler = new BlobController();
                      
                    break;

                case "basket":
                    var bll1 = Provider.FrontEndBll;
                    var client1 = await bll1.GetClientByRequest(request.Url);
                    controler = new BasketControler(client1);
                   
                    break;
                case "contact":
                    var bll2 = Provider.FrontEndBll;
                    var client2 = await bll2.GetClientByRequest(request.Url);
                    controler = new ContactControler(client2);
                  
                    break;
                default:
                    string group = request.QueryString["g"];
                    int groupID = 0;
                    if (!string.IsNullOrEmpty(group)&& id==0)
                    {
                        int.TryParse(group, out groupID);
                        id = groupID;
                    }
                    var bll = Provider.FrontEndBll;
                   var client = await bll.GetClientByRequest(request.Url);
                    controler = new HomeControler(client, path, id);
                

                    break;
            }

            controler.searchvalue = searchValue;
            controler.caseID = id;
            controler.currentPage = pageNumber;


            return controler;
        }
    }
}
