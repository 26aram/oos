﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;

namespace FrontEndML
{
    public class Converter
    {
        public static string CleanAll(string val)
        {
            if (!string.IsNullOrEmpty(val))
            {
                val = val.Trim();
                if (val.Length > 200)
                    val = val.Substring(0, 200);
                // return val.Trim().Replace("&", "-").Replace(".", "-").Replace(":", "-").Replace("/", "-").Replace(" ", "-").Replace(",", "-");
                val = Regex.Replace(val, @"[^\w\.@-]", "_", RegexOptions.None).Replace(".", "_").Replace("---", "_").Replace("--", "_");
            }
            return val;
        }



        public static List<Test> ReadCookie(string val)
        {
            var request = HttpContext.Current.Request;
            
            var cook = request.Cookies["shipments"];
            if (cook != null)
            {
                var sJson = cook.Value;
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();

                var t = Deserialize<List<Test>>(sJson);
                return t;
            }
            return null;
        }


        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }

    }


    public class Test
    {

        public int s { get; set; }
        public int q { get; set; }

    }
}