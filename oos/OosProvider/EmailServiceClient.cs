﻿using Facad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OosProvider
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IEmailServiceChannel : IEmailService, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class EmailServiceClient : System.ServiceModel.ClientBase<IEmailService>, IEmailService
    {

        public EmailServiceClient()
        {
        }

        public EmailServiceClient(string endpointConfigurationName) :
                base(endpointConfigurationName)
        {
        }

        public EmailServiceClient(string endpointConfigurationName, string remoteAddress) :
                base(endpointConfigurationName, remoteAddress)
        {
        }

        public EmailServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
                base(endpointConfigurationName, remoteAddress)
        {
        }

        public EmailServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        public Task<string> Send_EmailConfirmation_Email(List<string> to, string code)
        {
            return Task.Run(() => base.Channel.Send_EmailConfirmation_Email(to, code));
        }

        public Task<string> Send_TemporaryPassword_Email(List<string> to, string code)
        {
            return Task.Run(() => base.Channel.Send_TemporaryPassword_Email(to, code));
        }
        public Task<string> Send_PaswordChangedConfirmation_Email(List<string> to, string code)
        {
            return Task.Run(() => base.Channel.Send_PaswordChangedConfirmation_Email(to, code));
        }




    }
}
