﻿using BLL;
using Facad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OosProvider
{
    public class Provider
    {
        public static IBll BllService
        {
            get
            {
                return new BllProvider();
            }
        }

       

        public static IAddressService AddressService
        {
            get
            {
                return new AddressServiceClient();
            }
        }

        public static IEmailService EmailService
        {
            get
            {
                return new EmailServiceClient();
            }
        }
    }
}
