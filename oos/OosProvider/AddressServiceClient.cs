﻿using Facad;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OosProvider
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IAddressServiceChannel : IAddressService, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AddressServiceClient : System.ServiceModel.ClientBase<IAddressService>, IAddressService
    {

        public AddressServiceClient()
        {
        }

        public AddressServiceClient(string endpointConfigurationName) :
                base(endpointConfigurationName)
        {
        }

        public AddressServiceClient(string endpointConfigurationName, string remoteAddress) :
                base(endpointConfigurationName, remoteAddress)
        {
        }

        public AddressServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
                base(endpointConfigurationName, remoteAddress)
        {
        }

        public AddressServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        public string GetData(int value)
        {
            return base.Channel.GetData(value);
        }

      

        public Facad.DataContract.CompositeType GetDataUsingDataContract(Facad.DataContract.CompositeType composite)
        {
            return base.Channel.GetDataUsingDataContract(composite);
        }

       

        public Facad.DataContract.CompositeType VerifyAddress(Facad.DataContract.AddressModel composite)
        {
            return base.Channel.VerifyAddress(composite);
        }

       

        public Facad.DataContract.CompositeType VerifyUspsAddress(Facad.DataContract.AddressModel composite)
        {
            return base.Channel.VerifyUspsAddress(composite);
        }

       
    }
}
