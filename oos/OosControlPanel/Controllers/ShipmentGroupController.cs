﻿using Facad.DataContract;
using OosProvider.Helper.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OosControlPanel.Controllers
{
    [Authorize]
    public class ShipmentGroupController : BaseController
    {
        // GET: ShipmentGroup
        public async Task<ActionResult> Index(int id = 0)
        {


            var model = new Client();
            model.UserID = UserID;
            if (id > 0)
            {
                model.ClientID = id;
                model = await OosProvider.Provider.BllService.GetGroupsForClient(model);

                if (model.IsSuccess == IsSuccess.No)
                {
                    ModelState.AddModelError("", model.Message);
                }

            }


            if (model.Groups == null)
                model.Groups = new Dictionary<int, string>();



            return View(model);
        }


        #region Group Add/Edit
        [HttpPost]
        public async Task<ActionResult> AddGroupPopup(string name, int id)
        {

            StringBuilder sb = new StringBuilder();
            var model = new Client();
            model.UserID = UserID;

            if (id > 0)
            {
                var provider = OosProvider.Provider.BllService;
                model.ClientID = id;
                model.GroupName = name;
                model = await provider.SaveGroupForClient(model);

                var groups = await provider.GetGroupsForClient(model);

              

                foreach (var item in groups.Groups)
                {
                    sb.AppendFormat("<option value=\"{0}\">{1}</option>", item.Key.ToString(), item.Value);
                }
            }


            return Content(sb.ToString());
        }

        public async Task<ActionResult> AddGroup(int clientID, int GroupID)
        {


            var model = new Client();
            model.Company = "No@Name.No";
            model.CompanyAddress = "No@Name.No";
            model.CompanyEmail = "No@Name.No";
            if (ModelState.IsValid)
            {
                model.UserID = UserID;
                model.ClientID = clientID;

                var provider = OosProvider.Provider.BllService;

                if (GroupID > 0)
                {
                    model.GroupID = GroupID;
                    model = await provider.GetGroupForClient(model);
                    if (model.IsSuccess == IsSuccess.No)
                    {
                        ModelState.AddModelError("", model.Message);
                    }
                }
                
            }

            return PartialView(model);



        }

       


        [HttpPost]
        public async Task<ActionResult> AddGroup(Client model, int GroupID)
        {


            if (ModelState.IsValid)
            {
                model.UserID = UserID;
                model.GroupID = GroupID;

                model = await OosProvider.Provider.BllService.SaveGroupForClient(model);
                if (model.IsSuccess == IsSuccess.Yes)
                {
                    return RedirectToAction("Index", new { id = model.ClientID });
                }
                else
                {
                    ModelState.AddModelError("", model.Message);
                }
            }

            return View(model);
        }


        [HttpPost]
        public async Task<ActionResult> DeleteGroup(Client model, int GroupID = 0)
        {

            model.UserID = UserID;
            model = await OosProvider.Provider.BllService.DeleteGroupForClient(model, GroupID);
            return RedirectToAction("Index", new { id = model.ClientID });
        }

        #endregion
    }
}