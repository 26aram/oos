﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using OosControlPanel.Models;
using OosProvider.Helper.BaseClasses;
using Facad.DataContract;
using System.Security.Claims;
using System.Collections.Generic;

namespace OosControlPanel.Controllers
{
    [Authorize]
    public class AccountSettingController : BaseController
    {
        #region Index
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Index(User model, string returnUrl)
        {


            if (ModelState.IsValid)
            {
                // return View("Error");


                var result = await OosProvider.Provider.BllService.LoginUser(model);
                if (result.IsSuccess == IsSuccess.Yes)
                {
                   
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                }
            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {

            var userId = UserID;

            var result = await OosProvider.Provider.BllService.GetUserByGuid(userId);
            if (string.IsNullOrEmpty(result.Phone))
                result.Phone = "Missing";


            return View(result);
        }
        #endregion


        #region Change Phone
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ChangePhone(AddPhoneNumberViewModel model, string returnUrl)
        {


            if (ModelState.IsValid)
            {



                if (model.Phone != model.ConfirmPhone)
                {
                    ModelState.AddModelError("", "Phone And Confirm Phone do not match!");
                }
                else
                {

                    var userId = User.Identity.GetUserId();

                    var result = await OosProvider.Provider.BllService.SaveUserPthone(userId, model.Phone);
                    if (result.IsSuccess == IsSuccess.Yes)
                    {

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", result.Message);
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> ChangePhone()
        {
            AddPhoneNumberViewModel model = new AddPhoneNumberViewModel();


             var userId = User.Identity.GetUserId();

            var result = await OosProvider.Provider.BllService.GetUserByGuid(userId);
          
            model.Phone = result.Phone;


            return View(model);
        }
        #endregion


        #region Change Password
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (model.NewPassword != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Password And Confirm Password do not match!");
                }
                else
                {
                    var userId = User.Identity.GetUserId();
                    var result = await OosProvider.Provider.BllService.SaveUserPassword(userId, model);
                    if (result.IsSuccess == IsSuccess.Yes)
                    {
                        AuthenticateUser(result);

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", result.Message);
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            ChangePasswordViewModel model = new ChangePasswordViewModel();
            return View(model);
        }
        #endregion




        #region Helper
       
        private void AuthenticateUser(User model)
        {
            var claims = new List<Claim>();

            // create *required* claims
            claims.Add(new Claim(ClaimTypes.NameIdentifier, model.Guid));
            claims.Add(new Claim(ClaimTypes.Name, model.UserEmail));

            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);



            // add to user here!
            AuthenticationManager.SignIn(new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = model.IsActive,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            }, identity);

        }


        #endregion
    }
}
