﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using OosControlPanel.Models;
using Facad.DataContract;
using System.Collections.Generic;

namespace OosControlPanel.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
       

        #region Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new User { ConfirmPassword = "empaty" });
        }

       
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(User model, string returnUrl)
        {
           

            if (ModelState.IsValid)
            {
                // return View("Error");


                var result = await OosProvider.Provider.BllService.LoginUser(model);
                if (result.IsSuccess == IsSuccess.Yes)
                {
                    AuthenticateUser(result);
                    return RedirectToAction("Index", "Manage");
                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                }
            }
            return View(model);
        }
        #endregion




        #region Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View(new User());
        }

      
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(User model)
        {
            if (ModelState.IsValid)
            {
                var result = await OosProvider.Provider.BllService.RegistterUser(model);
                if (result.IsSuccess == IsSuccess.Yes)
                {
                    var toList = new List<string>();
                    toList.Add(result.UserEmail);
                    var provider = OosProvider.Provider.EmailService;
                    var emailresult = await provider.Send_EmailConfirmation_Email(toList, result.Guid.Substring(0, Facad.Constants.EmailConfirmationCodLenth));


                    return RedirectToAction("Confirm", "Account");
                }
                ModelState.AddModelError("", result.Message);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        #endregion



        #region Comfirm Code
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Confirm(User model)
        {
            if (ModelState.IsValid)
            {
                // return View("Error");


                var result = await OosProvider.Provider.BllService.LoginUserWithCode(model, model.ConfirmPassword);
                if (result.IsSuccess == IsSuccess.Yes)
                {
                    AuthenticateUser(result);
                    return RedirectToAction("Index", "Manage");
                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                }
            }
            return View(model);

        }

        [AllowAnonymous]
        public ActionResult Confirm()
        {
            return View(new User());
        }


        #endregion



        #region Forgot Password
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View(new User { ConfirmPassword = "empaty", UserPassword = "empaty" });
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(User model)
        {

            if (ModelState.IsValid)
            {
                var result = await OosProvider.Provider.BllService.SendForgotPasswordCode(model);
                if (result.IsSuccess == IsSuccess.Yes)
                {

                    var provider = OosProvider.Provider.EmailService;
                    var emailresult = await provider.Send_TemporaryPassword_Email(new List<string>{ model.UserEmail}, model.UserPassword);


                    return RedirectToAction("ResetPassword", "Account");

                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                }
            }
            return View(model);
        }

        #endregion

        #region ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            return View(new User());
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(User model)
        {

            if (ModelState.IsValid)
            {
                var result = await OosProvider.Provider.BllService.ResetPasswordFromForgotPassword(model);
                if (result.IsSuccess == IsSuccess.Yes)
                {

                    var provider = OosProvider.Provider.EmailService;
                    var emailresult = await provider.Send_PaswordChangedConfirmation_Email(new List<string> { model.UserEmail }, "");
                    AuthenticateUser(result);

                    return RedirectToAction("Index", "Manage");

                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                }
            }
            return View(model);
        }

        #endregion







        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie, DefaultAuthenticationTypes.ExternalCookie);
            return RedirectToAction("Index", "Home");
        }

      
        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }


        private void AuthenticateUser(User model)
        {
            var claims = new List<Claim>();

            // create *required* claims
            claims.Add(new Claim(ClaimTypes.NameIdentifier, model.Guid));
            claims.Add(new Claim(ClaimTypes.Name, model.UserEmail));

            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);



            // add to user here!
            AuthenticationManager.SignIn(new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = model.IsActive,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            }, identity);

        }
        #endregion
    }
}