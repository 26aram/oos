﻿using OosProvider.Helper.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OosControlPanel.Controllers
{
    public class BlobController : BaseController
    {
        // GET: Blob
        public async Task<ActionResult> Index(int id)
        {
            var result = await OosProvider.Provider.BllService.GetFile(id);
            return new FileContentResult(result, "image/png");
        }
    }
}