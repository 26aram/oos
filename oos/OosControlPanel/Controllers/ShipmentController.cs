﻿using Facad.DataContract;
using OosProvider.Helper.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OosControlPanel.Controllers
{
    [Authorize]
    public class ShipmentController : BaseController
    {
        public async Task<ActionResult> Index(int id=0)
        {


            var model = new Client();
            model.UserID = UserID;
            if (id > 0)
            {
                model.ClientID = id;
                model = await OosProvider.Provider.BllService.GetShipmentsForClient(model);

                if (model.IsSuccess == IsSuccess.No)
                {
                    ModelState.AddModelError("", model.Message);
                }

            }


            if (model.Shipments == null)
                model.Shipments = new System.Collections.Generic.List<Shipment>();



            return View(model);
        }


    
        public async Task<ActionResult> AddShipment(int clientID, int ShipmentID=0)
        {

            var model = new Client();
            model.Company = "No@Name.No";
            model.CompanyAddress = "No@Name.No";
            model.CompanyEmail = "No@Name.No";
            if (ModelState.IsValid)
            {
                model.UserID = UserID;
                model.ClientID = clientID;

                var provider = OosProvider.Provider.BllService;

                if (ShipmentID > 0)
                {
                    model = await provider.GetShipmentForClient(model, ShipmentID);
                    if (model.IsSuccess == IsSuccess.No)
                    {
                        ModelState.AddModelError("", model.Message);
                    }
                }
                else
                {
                    model.Shipment = new Shipment();
                }

                var groups = await provider.GetGroupsForClient(model);
                model.Groups = groups.Groups;
            }

            return PartialView(model);
        }


        [HttpPost]
        public async Task<ActionResult> AddShipment(Client model, int ShipmentID=0)
        {

            model.UserID = UserID;

            if (ModelState.IsValid)
            {
              
                model.Shipment.Image = ReadFileFromRequest("itmImg");

                model = await OosProvider.Provider.BllService.SaveShipmentForClient(model, ShipmentID);
                if (model.IsSuccess == IsSuccess.Yes)
                {
                    return RedirectToAction("Index", new { id = model.ClientID });
                }
                else
                {
                    ModelState.AddModelError("", model.Message);
                }
            }
            else
            {
                var groups = await OosProvider.Provider.BllService.GetGroupsForClient(model);
                model.Groups = groups.Groups;
                
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteShipment(Client model, int ShipmentID=0)
        {
        
                model.UserID = UserID;
                model = await OosProvider.Provider.BllService.DeleteShipmentForClient(model, ShipmentID);
                return RedirectToAction("Index", new { id = model.ClientID });
        }
    }
}