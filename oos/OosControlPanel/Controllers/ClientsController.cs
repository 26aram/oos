﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using OosControlPanel.Models;
using OosProvider.Helper.BaseClasses;
using Facad.DataContract;

namespace OosControlPanel.Controllers
{
    [Authorize]
    public class ClientsController : BaseController
    {
        // GET: Clients
        public async Task<ActionResult> Index()
        {


            var model = new Clients();
            model.UserID = UserID;

            var result = await OosProvider.Provider.BllService.GetClientsForUser(model);


            return View(model);
        }


        #region Client Add/Edit
        public async Task<ActionResult> AddClient(int id)
        {


            var model = new Client();
            model.UserID = UserID;
            if (id > 0)
            {
                model.ClientID = id;
                model = await OosProvider.Provider.BllService.GetClientForUser(model);
            }


            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> AddClient(Client model)
        {


            if (ModelState.IsValid)
            {
                model.UserID = UserID;
                model.LogoImage = ReadFileFromRequest("LogoFile");


                model = await OosProvider.Provider.BllService.SaveClientForUser(model);
                if (model.IsSuccess == IsSuccess.Yes)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", model.Message);
                }
            }

            return View(model);
        }
        #endregion


      
    }
}