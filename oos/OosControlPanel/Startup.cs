﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OosControlPanel.Startup))]
namespace OosControlPanel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
