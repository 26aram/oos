﻿

    $('.money').mask("#,##0.00", { reverse: true });
    $('.phone').mask('(000) 000-0000');


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image_upload_preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#inputFile").change(function () {
    readURL(this);
});

function readItemsURL(input, output) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#' + output).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function openConfirmDialog(postback) {
    var s = "<div id=\"confirmDialog\" title=\"Confirmation\"><p>Are you sure you want to delete this?</p></div>";
    var confirmDialog = document.getElementById("confirmDialog");
    if(!confirmDialog)
    {
        var $newdiv1 = $(s);
        $("body").append($newdiv1);
        confirmDialog = document.getElementById("confirmDialog");
    }
    $(function () {
   
    $(confirmDialog).dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Yes": function () {
                $(this).dialog("close");
                postback();
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });
    });
}

function addShipment() {
    var s="<table class=\"defTbl\" id=\"item\">";
   
    s+="<tbody><tr>";
    s+="<td class=\"col-md-2\" rowspan=\"4\">";
    s+="<img class=\"image_upload_preview\" id=\"image_upload_previewShipment\" alt=\"your image\">";
    s+="<div class=\"upload\">";
    
    s+="<input id=\"inputFileShipment\" onchange=\"readItemsURL(this, 'image_upload_previewShipment')\" type=\"file\">";
    
    s+="</div>";
    s+="</td>";
    
    s+="<td class=\"col-md-1\">";
    s+="<label class=\"control-label\" for=\"Name\">Name</label>";
    s+="</td>";
    s+="<td class=\"col-md-7\" colspan=\"3\">";
    s+="<div>";
    s += "<input name=\"itemName\"  maxlength=\"300\" class=\"itemLong\" id=\"itemName\" type=\"text\">";
    s+="</div>";
    s+="</td>";
    s+="</tr>";
    s+="<tr>";
    s+="<td>";
    s+="<label class=\"control-label\" for=\"Description\">Description</label>";
    s+="</td>";
    s+="<td colspan=\"3\">";
    s+="<div>";
    s += "<textarea  maxlength=\"500\" name=\"itemDescription\" class=\"itemArr\" id=\"itemDescription\" rows=\"2\" cols=\"20\"></textarea>";
    s+="</div>";
    s+="</td>";
    s+="</tr>";
    s+="<tr>";
    s+="<td>";
    s+="<label class=\"control-label\" for=\"Measure\">Measure</label>";
    s+="</td>";
    s+="<td>";
    s+="<div>";
    s += "<input name=\"itemUofM\" maxlength=\"12\" class=\"itemShort\" id=\"itemUofM\" type=\"text\">";
    s+="</div>";
    s+="</td>";
    s+="<td>";
    s+="<label class=\"control-label\" for=\"Price\">Price</label>";
    s+="</td>";
    s+="<td>";
    s+="<div>";
    s += "<input name=\"itemPrice\" maxlength=\"12\" class=\"itemShort money\" id=\"itemPrice\" type=\"text\" value=\"0\">";
    s+="</div>";
    s += "</td>";
    s += "</tr>";
    s += "<tr>";
    s+="<td>";
    s+="<div>";
    s += "<a class=\"btn btn-default\" onclick=\"openConfirmDialog(function(){removeShipment(event)})\" href=\"javascript:void(0)\">Delete</a>";
    s+="</div>";
    s+="</td>";
    s+="</tr>";
    s += "</tbody></table>";

  
    $("#Shipments").append(s);
    
    
}

function removeShipment(obj, c, s) {
   
    var sss = $('<form action="/Shipment/DeleteShipment?ShipmentID=' + s + '" enctype="multipart/form-data" method="post"><input name="ClientID" id="ClientID" type="hidden" value="' + c + '"></form>');
    sss.appendTo('body').submit();
}

function removeGroup(obj, c, g) {

    var sss = $('<form action="/ShipmentGroup/DeleteGroup?GroupID=' + g + '" enctype="multipart/form-data" method="post"><input name="ClientID" id="ClientID" type="hidden" value="' + c + '"></form>');
    sss.appendTo('body').submit();
}

function validateShipmentBeforSubmit(obj) {
    $('#shipmentForm').submit(function (e) {
        var drp = document.getElementById("Shipment_GroupOfShipmentID");
        if (!drp.value) {
            e.preventDefault(e);
            drp.style.borderColor = "red";
        }
    });
 
}

function CallDialog(obj, url, data, postback)
{
    var s = "<div id=\"modalDialog\" title=\"" + obj.title + "\"></div>";
    var modalDialog = document.getElementById("modalDialog");
    if (!modalDialog) {
        var $newdiv1 = $(s);
        $("body").append($newdiv1);
        modalDialog = document.getElementById("modalDialog");
    }
  

    var title = document.getElementById("ui-id-1");
    if (title)
        title.innerText = obj.title;

    modalDialog.title = obj.title;

    $(modalDialog).empty();
    $.get(url + '&' + new Date().getTime(), data)
  .done(function (data) {
    
      $(modalDialog).append(data);
      if (postback) {
          $(modalDialog).dialog({
              resizable: false,
              height: "auto",
              modal: true,
              width: "auto",
              buttons: {
                  "Yes": function () {
                      $(this).dialog("close");
                      postback();
                  },
                  "No": function () {
                      $(this).dialog("close");
                  }
              }
          });
      }
      else {
          $(modalDialog).dialog({
              resizable: false,
              height: "auto",
              width: "auto",
              modal: true

          });

        
      }

    


      $('.pogh').on('keydown', fnCurrencyOnly)
                        .on('blur', 
                                 function () { 
                                    
                                 }
                              );
     
      validateShipmentBeforSubmit();
     
  })
 .fail(function () {
        alert("error");
    });
}





function prepearAddSipmentSubmitValidation() {
    alert("aaaaaaaaa");
}

function OpenAddGroupDialog(obj, url, data, postback)
{
    var $drp = $('#Shipment_GroupOfShipmentID');


    var s = "<div class=\"tip\" id=\"addGroup\">Add New Group<table class=\"tblTip\"><tr><td colspan=\"2\"><input id=\"newGroupName\" name=\"newGroupName\" type=\"text\"></td></tr><tr><td><input class=\"btn-smol\" onclick=\"saveGroup(this)\" type=\"button\" value=\"Save\"></td><td><input class=\"btn-smol\" onclick=\"cancelGroup(this)\" type=\"button\" value=\"Cansel\"></td></tr></table></div></div>";


    var modalDialog = document.getElementById("addGroup");
    if (!modalDialog) {
        var $newdiv1 = $(s);
        $drp.parent().append($newdiv1);
        modalDialog = document.getElementById("addGroup");
    }
    modalDialog.style.display = "block";
    document.getElementById("newGroupName").value = "";
    document.getElementById("newGroupName").style.borderColor = "green";
    var pos = $drp.position();
    var w = $drp.width();
    $(modalDialog).css({ top: 0, left: pos.left, position: 'absolute' }).width(w);


  
}
function closeModalDialog(obj) {
    $('#' +obj).dialog("close");
}
function cancelGroup(obj)
{
    var modalDialog = document.getElementById("addGroup");
    modalDialog.style.display = "none";
}
function saveGroup(obj)
{
    var newGroup = document.getElementById("newGroupName");
    if (newGroup.value) {
        var modalDialog = document.getElementById("addGroup");
        var drp = document.getElementById("Shipment_GroupOfShipmentID");
        $.post("/ShipmentGroup/AddGroupPopup", { name: document.getElementById("newGroupName").value, id: document.getElementById("ClientID").value })
        .done(function (data) {
            drp.innerHTML = data;
            $("#Shipment_GroupOfShipmentID option:last").attr("selected", "selected");
            cancelGroup();
        });
    }
    else {
        newGroup.style.borderColor = "red";
    }
}

///////////////

// JavaScript I wrote to limit what types of input are allowed to be keyed into a textbox 
var allowedSpecialCharKeyCodes = [46, 8, 37, 39, 35, 36, 9];
var numberKeyCodes = [44, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
var commaKeyCode = [188];
var decimalKeyCode = [190, 110];

function numbersOnly(event) {
    var legalKeyCode =
        (!event.shiftKey && !event.ctrlKey && !event.altKey)
            &&
        (jQuery.inArray(event.keyCode, allowedSpecialCharKeyCodes) >= 0
            ||
        jQuery.inArray(event.keyCode, numberKeyCodes) >= 0);

    if (legalKeyCode === false)
        event.preventDefault();
}

function numbersAndCommasOnly(event) {
    var legalKeyCode =
        (!event.shiftKey && !event.ctrlKey && !event.altKey)
            &&
        (jQuery.inArray(event.keyCode, allowedSpecialCharKeyCodes) >= 0
            ||
        jQuery.inArray(event.keyCode, numberKeyCodes) >= 0
            ||
        jQuery.inArray(event.keyCode, commaKeyCode) >= 0);

    if (legalKeyCode === false)
        event.preventDefault();
}

function decimalsOnly(event) {
    var legalKeyCode =
        (!event.shiftKey && !event.ctrlKey && !event.altKey)
            &&
        (jQuery.inArray(event.keyCode, allowedSpecialCharKeyCodes) >= 0
            ||
        jQuery.inArray(event.keyCode, numberKeyCodes) >= 0
            ||
        jQuery.inArray(event.keyCode, commaKeyCode) >= 0
            ||
        jQuery.inArray(event.keyCode, decimalKeyCode) >= 0);

    if (legalKeyCode === false)
        event.preventDefault();
}

function currenciesOnly(event) {
    var legalKeyCode =
        (!event.shiftKey && !event.ctrlKey && !event.altKey)
            &&
        (jQuery.inArray(event.keyCode, allowedSpecialCharKeyCodes) >= 0
            ||
        jQuery.inArray(event.keyCode, numberKeyCodes) >= 0
            ||
        jQuery.inArray(event.keyCode, commaKeyCode) >= 0
            ||
        jQuery.inArray(event.keyCode, decimalKeyCode) >= 0);

    // Allow for $
    if (!legalKeyCode && event.shiftKey && event.keyCode == 52)
        legalKeyCode = true;

    if (legalKeyCode === false)
        event.preventDefault();
}


function CurrencyFormat(e) {
    var val = e.currentTarget.value;
    var key = e.keyCode || e.charCode || e.which;
    var currentChar = String.fromCharCode(key);
    if (e.keyCode == 46 && e.charCode == 0 && e.which == 0) {
        $(this).val("");
        return true;
    }
    if (e.keyCode == 36 && e.charCode == 0 && e.which == 0) {
        $(this).val("");
        return true;
    }
    if (val.indexOf(currentChar) != -1 && currentChar == ".") {
        return false;
    }
    if (val.indexOf(currentChar) != -1 && currentChar == "$") {
        return false;
    }
    if (key >= 48 && key <= 57 || key == 46 || key == 36 || e.keyCode === 8 || e.keyCode === 9 || e.keyCode === 37 || e.keyCode === 35 || e.keyCode === 39) {
        $(this).val("");
        return true;
    }
    return false;
}






function fnCurrencyOnly(event) {
    if (event.shiftKey === true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57)
        || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8
        || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 110) {
        if (event.keyCode == 110) {
            var val = event.currentTarget.value;
            if(val.indexOf('.')>=0)
                event.preventDefault();
        }
    }
    else {
            event.preventDefault();
        }
}



//////////////////


$(document).ready(
    function()
    {
       
    });
