﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndBLL
{
    public class BlobManager: BaseManager
    {
        public static byte[] GetBlob(int blobID)
        {
            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Blobs.FirstOrDefault(f => f.BlobID == blobID);
                    if (u != null)
                    {

                        return u.BlobData;
                    }
                }
                catch (Exception ex)
                {

                    HandleException(ex);
                }
            }

            return null;
        }
    }
}
