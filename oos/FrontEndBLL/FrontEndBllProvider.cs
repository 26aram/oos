﻿using Facad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndBLL
{
    public class FrontEndBllProvider : IFrontEndBll
    {
        public Task<Facad.DataContract.Client> GetClientByRequest(Uri myUri)
        {
            var Result = Task.Run(() => ClientManager.GetClientByRequest(myUri));
            Result.Wait();
            return Result;
        }


        public Task<List<Facad.DataContract.Shipment>> GetShipmentsForClient(int clientID, string serchVal, int pageNumber, int groupID)
        {
            var Result = Task.Run(() => ShipmentManager.GetShipmentsForClient(clientID, serchVal, pageNumber, groupID));
            Result.Wait();
            return Result;
        }


        public Task<byte[]> GetBlobById(int blobID)
        {
            var Result = Task.Run(() => BlobManager.GetBlob(blobID));
            Result.Wait();
            return Result;


        }

        public Task<Dictionary<int, string>> GetGroupsForClient(int clientId)
        {
            var Result = Task.Run(() => GroupManager.GetGroupsForClient(clientId));
            Result.Wait();
            return Result;
        }

        public Task<List<Facad.DataContract.Shipment>> GetShipmentsSelectedByUser(List<int> shipmentList)
        {
            var Result = Task.Run(() => ShipmentManager.GetShipmentsSelectedByUser(shipmentList));
            Result.Wait();
            return Result;
        }

    }
}

