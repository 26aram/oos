﻿using DAL;
using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FrontEndBLL
{
    [DataContract]
    public class DomainDescriptor
    {
     
        [DataMember]
        public int DomainID { get; set; }

        [DataMember]
        public string DomainName { get; set; }

        [DataMember]
        public bool IsTest { get; set; }

    }

    public class ClientManager:BaseManager
    {
        public const string domainParam = "domainid";
       




        public static  Facad.DataContract.Client GetClientByRequest(Uri myUri)
        {
            DomainDescriptor descriptor = GetDescriptor(myUri);
            Facad.DataContract.Client client = new Facad.DataContract.Client();

            #region Test Domain With ID
            if (descriptor.DomainID > 0)
            {
                using (OOSEntities m = new OOSEntities())
                {
                    try
                    {
                        var u = m.Clients.FirstOrDefault(f => f.ClientID == descriptor.DomainID);
                        if (u != null)
                        {
                            FromEntityToDto(client, u);
                            client.IsTest = true;

                            #region geting Groups
                            var Result = Task.Run(() => GroupManager.GetGroupsForClient(client.ClientID));
                            Result.Wait();
                            client.Groups = Result.Result;
                            #endregion
                           
                        }
                    }
                    catch (Exception ex)
                    {

                        HandleException(ex);
                    }
                }
            }
            #endregion
            #region Not Test With DomainName
            else
            {
                using (OOSEntities m = new OOSEntities())
                {
                    try
                    {
                        var u = m.Clients.FirstOrDefault(f => f.Domain == descriptor.DomainName && f.IsActive && !f.IsSuspended);
                        if (u != null)
                        {
                            FromEntityToDto(client, u);
                            client.IsTest = false;

                            #region geting Groups
                            var Result = Task.Run(() => GroupManager.GetGroupsForClient(client.ClientID));
                            Result.Wait();
                            client.Groups = Result.Result;
                            #endregion

                           
                        }
                    }
                    catch (Exception ex)
                    {

                        HandleException(ex);
                    }
                }
            }

            #endregion

            return client;
        }


        private static void FromEntityToDto(Facad.DataContract.Client dto, DAL.Client entity)
        {
            dto.BackgroundColor = entity.BackgroundColor;
            dto.ClientID = entity.ClientID;
            dto.Company = entity.Company;
            dto.CompanyAddress = entity.CompanyAddress;
            dto.DeliveryNote = entity.DeliveryNote;
            dto.CompanyEmail = entity.CompanyEmail;
            dto.CompanyPhone = entity.CompanyPhone;
            dto.Domain = entity.Domain;
            dto.IncludeCompanyEmail = entity.IncludeCompanyEmail;
            dto.IncludeCompanyName = entity.IncludeCompanyName;
            dto.IncludeCompanyPhone = entity.IncludeCompanyPhone;
            dto.LogoImageID = entity.LogoImageID.GetValueOrDefault(0);
            dto.PreparationTime = entity.PreparationTime;
            dto.HeaderColor = entity.HeaderColor;
        }

        private static DomainDescriptor GetDescriptor(Uri myUri)
        {
            string hostpart = myUri.Authority;
            DomainDescriptor descriptor = new DomainDescriptor();
          
            string param1 = HttpUtility.ParseQueryString(myUri.Query).Get(domainParam);
            int domainID = 0;
            if (int.TryParse(param1, out domainID))
            {
                descriptor.DomainID = domainID;
                descriptor.IsTest = true;
            }
            else
            {
                descriptor.DomainName = hostpart;
                descriptor.IsTest = false;
            }
            return descriptor;
        }
    }
}
