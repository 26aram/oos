﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndBLL
{
   

    public class GroupManager : BaseManager
    {
        public static Dictionary<int, string> GetGroupsForClient(int clientID)
        {
            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.GroupOfShipments.Where(w => w.IsActive && w.ClientID == clientID).ToDictionary(g => g.GroupOfShipmentID, g => g.GroupName);

                    return u;
                }
                catch (Exception ex)
                {

                    HandleException(ex);
                }
            }

            return null;
        }
    }
}
