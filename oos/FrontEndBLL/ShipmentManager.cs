﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndBLL
{
   

    public class ShipmentManager : BaseManager
    {
        public static List<Facad.DataContract.Shipment> GetShipmentsForClient(int clientID, string serchVal, int pageNumber, int groupID)
        {
            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var qsl = m.Shipments.Where(w => w.ClientID == clientID);
                    if (!string.IsNullOrEmpty(serchVal))
                    {
                        qsl = qsl.Where(w => w.Name.Contains(serchVal));
                    }
                    if(groupID>0)
                    {
                        qsl = qsl.Where(w => w.GroupOfShipmentID == groupID);
                    }


                    var u = qsl.OrderBy(o=>o.Name).Skip(BaseManager.PaginationItemCount*pageNumber).Take(BaseManager.PaginationItemCount).Select(s => new Facad.DataContract.Shipment
                    {

                        Name = s.Name,
                        ClientID = s.ClientID,
                       // CreatedDate = s.CreatedDate,
                        Description = s.Description,
                        Price = s.Price,
                        ShipmentID = s.ShipmentID,
                        UofM = s.UofM,
                        GroupOfShipmentID = s.GroupOfShipmentID,
                        ImageID = s.ImageID.HasValue?s.ImageID.Value:0,
                    }).ToList();

                   

                    return u;
                }
                catch (Exception ex)
                {

                    HandleException(ex);
                }
            }

            return null;
        }

        public static List<Facad.DataContract.Shipment> GetShipmentsSelectedByUser(List<int> shipmentList)
        {
            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Shipments.Where(w => shipmentList.Contains(w.ShipmentID)).Select(s => new Facad.DataContract.Shipment
                    {

                        Name = s.Name,
                        ClientID = s.ClientID,
                        // CreatedDate = s.CreatedDate,
                        Description = s.Description,
                        Price = s.Price,
                        ShipmentID = s.ShipmentID,
                        UofM = s.UofM,
                        GroupOfShipmentID = s.GroupOfShipmentID,
                        ImageID = s.ImageID.HasValue ? s.ImageID.Value : 0,
                    }).ToList();

                    return u;
                }
                catch (Exception ex)
                {

                    HandleException(ex);
                }
            }

            return null;
        }
    }
}
