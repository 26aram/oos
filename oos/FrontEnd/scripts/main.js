﻿function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function add(obj) {
    var inputs = obj.parentNode.getElementsByTagName('input');
    if (inputs && inputs.length>0) {
        if (inputs[0].value) {
            var o = new Object();
            o.q = inputs[0].value;
            o.s = obj.id;
            writeInMem(o);
            inputs[0].value = "";
           
        }
    }
}

function writeInMem(obj) {
    var cookieValue = [];
    var svat = getCookie("shipments");
    if (svat)
    cookieValue = JSON.parse(svat);
    var existed = getSameItem(obj, cookieValue);
    if (existed) {
        existed.q = parseInt(existed.q) + parseInt(obj.q);
    }
    else {
        cookieValue.push(obj);
    }
    setCookie("shipments", JSON.stringify(cookieValue), 100);
    document.getElementById("bskt").innerHTML = cookieValue.length;
}

function removeFromMem(id) {
    var cookieValue = [];
    var newCookieValue = [];
    var svat = getCookie("shipments");
    if (svat)
        cookieValue = JSON.parse(svat);

    for (var i = 0; i < cookieValue.length; i++) {
        if(cookieValue[i].s!=id)
            newCookieValue.push(cookieValue[i]);
    }

  
    setCookie("shipments", JSON.stringify(newCookieValue), 100);
    document.getElementById("bskt").innerHTML = newCookieValue.length;
}

function changeCountInMem(id, count) {
    var cookieValue = [];
    var svat = getCookie("shipments");
    if (svat)
        cookieValue = JSON.parse(svat);

    for (var i = 0; i < cookieValue.length; i++) {
        if (cookieValue[i].s == id)
        {
            cookieValue[i].q = count;
        }
    }


    setCookie("shipments", JSON.stringify(cookieValue), 100);
    document.getElementById("bskt").innerHTML = cookieValue.length;
}


function getSameItem(obj, list) {
    var s = obj.s;
    for (var i = 0; i < list.length; i++) {
        if (list[i].s == s)
            return list[i];
    }

    return null;
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function countTotal(obj) {
    var parent = obj.parentNode;
    var total = parent.getElementsByClassName("total");
    var price = parent.getElementsByClassName("price");
    if(total && total.length>0 && price && price.length>0)
    {
        var t = parseFloat(price[0].value) * parseInt(obj.value);
        total[0].innerText = '$' + t;

        var shipment = parent.getElementsByClassName("shipment");
        if(shipment && shipment.length>0)
        changeCountInMem(shipment[0].value, parseInt(obj.value));
    }
}

function deleteItem(obj) {
    promptWindow(obj);
}
function promptWindow(obj) {

    var cover = document.createElement("div");
    cover.className = 'alertify-cover';

    // Create template
    var box = document.createElement("div");
    box.className = 'modalConfirm'
  //  cover.appendChild(box);



    var nav = document.createElement("nav");
    box.appendChild(nav);

    var cancel = document.createElement("button");
    cancel.innerHTML = "Cancel";
    cancel.onclick = function ()
    {
        document.body.removeChild(box);
        document.body.removeChild(cover);
    }


    var ok = document.createElement("button");
    ok.innerHTML = "Ok";
    ok.el = obj;
    ok.onclick = function (a,b,c) {
        //document.body.removeChild(this.parentNode)

        var el = this.el.parentNode.parentNode;
        var pr = el.parentNode;
        pr.removeChild(el);
        removeFromMem(this.el.id);

        document.body.removeChild(box);
        document.body.removeChild(cover);
    }

    var br1 = document.createElement("br");
    nav.appendChild(br1);
    var br2 = document.createElement("br");
    nav.appendChild(br2);

    var text = document.createTextNode("Are you sure you want to delete this?");
    nav.appendChild(text);

    var br3 = document.createElement("br");
    nav.appendChild(br3);
    var br4 = document.createElement("br");
    nav.appendChild(br4);

  
   
    nav.appendChild(cancel);
    nav.appendChild(ok);

    // Style box
    box.style.position = "absolute";
    box.style.width = "300px";
    box.style.height = "150px";

    // Center box.
    box.style.left = ((window.innerWidth / 2) - 100) + 'px';
  
    box.style.top = ((window.innerHeight / 2) - 100) + 'px';

    // Append box to body
    document.body.appendChild(cover);
    document.body.appendChild(box);

}