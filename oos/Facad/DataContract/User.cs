﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Facad.DataContract
{
    [DataContract]
    public class User: BaseDataContract
    {
      
        [DataMember]
        [Required]
        [EmailAddress]
        [StringLength(300, ErrorMessage = "The {0} must be at least {2} characters long.{1}", MinimumLength = 6)]
        [Display(Name = "User Email")]
        public string UserEmail { get; set; }


        [DataMember]
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string UserPassword { get; set; }

        [DataMember]
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        [DataMember]
        public bool IsSuspended { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string Guid { get; set; }

        [DataMember]
        public string Phone { get; set; }


        public Client Client { get; set; }

    }


    [DataContract]
    public class AddPhoneNumberViewModel
    {
        [DataMember]
        [Required(ErrorMessage = "Your must provide a Phone Number")]
        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string Phone { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Your must provide a Confirm Phone Number")]
        [Display(Name = "Confirm Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string ConfirmPhone { get; set; }
    }
    [DataContract]
    public class ChangeEmailViewModel
    {
        [DataMember]
        [Required]
        [EmailAddress]
        [StringLength(300, ErrorMessage = "The {0} must be at least {2} characters long.{1}", MinimumLength = 6)]
        [Display(Name = "User Email")]
        public string Email { get; set; }

        [DataMember]
        [Required]
        [EmailAddress]
        [StringLength(300, ErrorMessage = "The {0} must be at least {2} characters long.{1}", MinimumLength = 6)]
        [Display(Name = "User Email")]
        public string ConfirmEmail { get; set; }
    }


    [DataContract]
    public class ChangePasswordViewModel
    {
        [DataMember]
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }
        [DataMember]
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }
        [DataMember]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

}
