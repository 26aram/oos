﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Facad.DataContract
{
    [DataContract]
    public enum MessageTypeEnum
    {
        [EnumMember]
        ComfirmEmail = 1,
        [EnumMember]
        SendTemporaryPassword = 2,

        [EnumMember]
        PasswordCangedConfirmation = 3,
    };
}
