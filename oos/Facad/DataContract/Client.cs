﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Facad.DataContract
{
    [DataContract]
    public class Client: BaseDataContract
    {

        [DataMember]
        public int ClientID { get; set; }

        [DataMember]
        [Required]
        public string Company { get; set; }

        [DataMember]
        [Required]
        [EmailAddress]
        [StringLength(300, ErrorMessage = "The {0} must be at least {2} characters long.{1}", MinimumLength = 6)]
        [Display(Name = "Company Email")]
        public string CompanyEmail { get; set; }
        [DataMember]
        public string CompanyPhone { get; set; }


        [DataMember]
        public string DeliveryNote { get; set; }

        [DataMember]
        [Required]
        public string CompanyAddress { get; set; }
        [DataMember]
        public System.DateTime CreatedDate { get; set; }
        [DataMember]
        public bool IsSuspended { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string Guid { get; set; }



        private string ___Domain;
        [DataMember]
        [StringLength(3000, ErrorMessage = "The Name can not be more then 3000 characters")]
        public string Domain
        {
            get
            {
                if (___Domain == null)
                    ___Domain = string.Empty;

                return ___Domain;
            }
            set
            {
                ___Domain = value;
            }
        }
        [DataMember]
        public string PreparationTime { get; set; }
        [DataMember]
        public string BackgroundColor { get; set; }

        [DataMember]
        public string HeaderColor { get; set; }
        [DataMember]
        public int LogoImageID { get; set; }
        [DataMember]
        public byte[] LogoImage { get; set; }
        [DataMember]
        public bool IncludeCompanyName { get; set; }
        [DataMember]
        public bool IncludeCompanyEmail { get; set; }
        [DataMember]
        public bool IncludeCompanyPhone { get; set; }

        public List<Shipment> Shipments { get; set; }

        public Shipment Shipment { get; set; }

        public int ShipmentID
        {
            get
            {
                if (Shipment != null)
                    return Shipment.ShipmentID;
                return 0;
            }
        }

        [DataMember]
        public Dictionary<int, string> Groups { get; set; }


        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public int GroupID { get; set; }

        public bool IsTest { get; set; }

    }
}
