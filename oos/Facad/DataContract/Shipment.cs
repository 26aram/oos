﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Facad.DataContract
{
    [DataContract]
    public partial class Shipment
    {
        [DataMember]
        public int ShipmentID { get; set; }
        [DataMember]
        public int ClientID { get; set; }
        [DataMember]

        [Required]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]

        [Required]
        public decimal Price { get; set; }
        [DataMember]
        [Required]
        public string UofM { get; set; }
        [DataMember]
        public System.DateTime CreatedDate { get; set; }

        [DataMember]
        public byte[] Image { get; set; }

        [DataMember]
        public int ImageID { get; set; }
        [DataMember]
        [Required]
        public Nullable<int> GroupOfShipmentID { get; set; }



        [DataMember]
        public Dictionary<int, string> Groups { get; set; }

    }
}
