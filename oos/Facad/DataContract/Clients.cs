﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facad.DataContract
{
    public class Clients: BaseDataContract
    {
      
        public List<Client> UserClients { get; set; }
    }

}
