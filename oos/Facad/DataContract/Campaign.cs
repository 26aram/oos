﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Facad.DataContract
{

    [DataContract]
    public partial class Campaign:BaseDataContract
    {
        public Campaign()
        {
            this.Shipments = new  List<Shipment>();
        }
        [DataMember]
        public int CampaignID { get; set; }
        [DataMember]
        public Nullable<int> ClientID { get; set; }
        [DataMember]
        [Required]
        [StringLength(300, ErrorMessage = "The Name can not be more then 300 characters")]
        public string Name { get; set; }
        [DataMember]
        [StringLength(3000, ErrorMessage = "The Name can not be more then 3000 characters")]
        public string Domain { get; set; }
        [DataMember]
        public string PreparationTime { get; set; }
        [DataMember]
        public string BackgroundColor { get; set; }

        [DataMember]
        public string HeaderColor { get; set; }
        [DataMember]
        public byte[] LogoImage { get; set; }
        [DataMember]
        public bool IncludeCompanyName { get; set; }
        [DataMember]
        public bool IncludeCompanyEmail { get; set; }
        [DataMember]
        public bool IncludeCompanyPhone { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool IsSuspended { get; set; }
        [DataMember]
        public System.DateTime CreatedDate { get; set; }
        [DataMember]
        public List<Shipment> Shipments { get; set; }
    }
}
