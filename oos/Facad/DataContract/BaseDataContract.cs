﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Facad.DataContract
{
    [DataContract]
    public class BaseDataContract
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public IsSuccess IsSuccess { get; set; }

        [DataMember]
        public string UserID { get; set; }
    }
    [DataContract]
    public enum IsSuccess
    {
        [EnumMember]
        Yes=1,
        [EnumMember]
        No =2
    };
}
