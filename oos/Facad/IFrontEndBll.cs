﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facad
{
    public interface IFrontEndBll
    {
        Task<Facad.DataContract.Client> GetClientByRequest(Uri myUri);

        Task<byte[]> GetBlobById(int blobID);

        Task<Dictionary<int, string>> GetGroupsForClient(int clientId);
        Task<List<Facad.DataContract.Shipment>> GetShipmentsForClient(int clientID, string serchVal, int pageNumber, int groupID);
        Task<List<Facad.DataContract.Shipment>> GetShipmentsSelectedByUser(List<int> shipmentList);
    }
}
