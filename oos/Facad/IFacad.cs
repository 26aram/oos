﻿using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facad
{
    public interface IBll
    {
        Task<User> RegistterUser(User user);

        Task<User> LoginUserWithCode(User user, string code);

        Task<User> LoginUser(User user);
        Task<User> SendForgotPasswordCode(User user);
        Task<User> ResetPasswordFromForgotPassword(User user);

        Task<User> GetUserByGuid(string userID);
        Task<User> SaveUserPthone(string userID, string phone);

        Task<User> SaveUserPassword(string userID, ChangePasswordViewModel model);

            
            
            
            
       Task<Clients> GetClientsForUser(Clients c);
       Task<Client> GetClientForUser(Client c);
       Task<Client> SaveClientForUser(Client c);

        Task<Client> GetShipmentsForClient(Client c);
        Task<Client> GetShipmentForClient(Client c, int shipmentID);

        Task<Client> GetGroupsForClient(Client c);
        Task<Client> GetGroupForClient(Client c);
        Task<Client> SaveGroupForClient(Client c);
        Task<Client> DeleteGroupForClient(Client c, int GroupID);


        Task<Client> SaveShipmentForClient(Client c, int shipmentID);
        Task<Client> DeleteShipmentForClient(Client c, int shipmentID);

        Task<byte[]> GetFile(int BlobID);
    }
}
