﻿using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Facad
{
    [ServiceContract]
    public interface IEmailService
    {


        [OperationContract]
        Task<string>  Send_EmailConfirmation_Email(List<string> to, string code);
        [OperationContract]
        Task<string> Send_TemporaryPassword_Email(List<string> to, string code);
        [OperationContract]
        Task<string> Send_PaswordChangedConfirmation_Email(List<string> to, string code);
    }
}
