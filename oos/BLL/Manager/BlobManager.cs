﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Manager
{

    public class BlobManager : BaseManager
    {
        public int? AddEditBlob(int BlobID, byte[] LogoImage)
        {
            int? blobID = null;
            if (LogoImage != null && LogoImage.Length>0)
            {
                using (OOSEntities m = new OOSEntities())
                {
                    try
                    {
                        var blob = m.Blobs.FirstOrDefault(f => f.BlobID == BlobID);
                        if (blob == null)
                        {
                            blob = new Blob();
                            blob.BlobData = LogoImage;
                            m.Blobs.Add(blob);
                        }
                        else
                        {
                            blob.BlobData = LogoImage;
                        }
                        m.SaveChanges();
                        blobID = blob.BlobID;
                    }
                    catch (Exception ex)
                    {

                        this.HandleException(ex, null);
                    }
                }
            }
            return blobID;
        }

        public byte[] GetFile(int BlobID)
        {
           
            if (BlobID>0)
            {
                using (OOSEntities m = new OOSEntities())
                {
                    try
                    {
                        var blob = m.Blobs.FirstOrDefault(f => f.BlobID == BlobID);
                        if (blob == null)
                        {
                            return null;
                        }
                        else
                        {
                            return blob.BlobData;
                        }
                       
                    }
                    catch (Exception ex)
                    {

                        this.HandleException(ex, null);
                    }
                }
            }
            return null;
        }
    }
}