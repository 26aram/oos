﻿using DAL;
using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Manager
{
    public class ClientManager : BaseManager
    {
        public Facad.DataContract.Clients GetClientsForUser(Clients c)
        {
            string userID = c.UserID;
            if (string.IsNullOrEmpty(userID))
            {
                WriteExcaptionMessage(c, "Unknown User.");
                return c;
            }

            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = m.Clients.Where(w => w.UserID == u.UserID).Select(s => new Facad.DataContract.Client
                        {
                            Company = s.Company,
                            CompanyEmail = s.CompanyEmail,
                            CompanyPhone = s.CompanyPhone,
                            DeliveryNote = s.DeliveryNote,
                            CompanyAddress = s.CompanyAddress,
                            ClientID = s.ClientID,
                            IsActive = s.IsActive,
                            Guid = s.Guid,
                            BackgroundColor = s.BackgroundColor,
                            CreatedDate = s.CreatedDate,
                            Domain = s.Domain,
                            HeaderColor = s.HeaderColor,
                            IncludeCompanyEmail = s.IncludeCompanyEmail,
                            IncludeCompanyName = s.IncludeCompanyName,
                            IncludeCompanyPhone = s.IncludeCompanyPhone,
                            IsSuspended = s.IsSuspended,
                            //LogoImage = s.LogoImage
                            PreparationTime = s.PreparationTime,
                            //UserID = s.UserID,

                        });

                        c.UserClients = cc.ToList();
                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }


        public Facad.DataContract.Client GetClientForUser(Facad.DataContract.Client c)
        {
            string userID = c.UserID;
            int clientid = c.ClientID;

            if (string.IsNullOrEmpty(userID))
            {
                WriteExcaptionMessage(c, "Unknown User.");
                return c;
            }

            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                        if(cc!=null)
                        {
                            c.Company = cc.Company;
                            c.CompanyEmail = cc.CompanyEmail;
                            c.CompanyPhone = cc.CompanyPhone;
                            c.DeliveryNote = cc.DeliveryNote;
                            c.CompanyAddress = cc.CompanyAddress;
                            c.CreatedDate = cc.CreatedDate;
                            c.Guid = cc.Guid;
                            c.IsActive = cc.IsActive;


                            c.Domain = cc.Domain;
                            c.HeaderColor = cc.HeaderColor;
                            c.IncludeCompanyEmail = cc.IncludeCompanyEmail;
                            c.IncludeCompanyName = cc.IncludeCompanyName;
                            c.IncludeCompanyPhone = cc.IncludeCompanyPhone; 
                            c.LogoImageID = cc.LogoImageID.GetValueOrDefault(0);
                            c.PreparationTime = cc.PreparationTime;
                            c.BackgroundColor = cc.BackgroundColor;
                        }
                        else
                        {
                            WriteExcaptionMessage(c, "Can Not Find Company.");
                            return c;
                        }
                      
                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }


        public Facad.DataContract.Client SaveClientForUser(Facad.DataContract.Client c)
        {
            string userID = c.UserID;
            int clientid = c.ClientID;

            if (string.IsNullOrEmpty(userID))
            {
                WriteExcaptionMessage(c, "Unknown User.");
                return c;
            }

            if (string.IsNullOrEmpty(c.Company))
            {
                WriteExcaptionMessage(c, "Company Name Can not be empaty.");
                return c;
            }

            if (string.IsNullOrEmpty(c.CompanyEmail))
            {
                WriteExcaptionMessage(c, "Company Email Can not be empaty.");
                return c;
            }

            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        if (clientid == 0)
                        {
                            DAL.Client cToAdd = new DAL.Client();
                            cToAdd.Company = c.Company;
                            cToAdd.CompanyEmail = c.CompanyEmail;
                            cToAdd.DeliveryNote = c.DeliveryNote;
                            cToAdd.CompanyPhone = c.CompanyPhone;
                            cToAdd.CompanyAddress = c.CompanyAddress;
                            cToAdd.CreatedDate = DateTime.Now;
                            cToAdd.Guid = Guid.NewGuid().ToString("N");
                            cToAdd.IsActive = c.IsActive;

                            cToAdd.Domain = c.Domain;
                            cToAdd.HeaderColor = c.HeaderColor;
                            cToAdd.IncludeCompanyEmail = c.IncludeCompanyEmail;
                            cToAdd.IncludeCompanyName = c.IncludeCompanyName;
                            cToAdd.IncludeCompanyPhone = c.IncludeCompanyPhone;
                            if (c.LogoImage != null && c.LogoImage.Length > 0)
                                cToAdd.LogoImageID = new BlobManager().AddEditBlob(c.LogoImageID, c.LogoImage);
                            cToAdd.PreparationTime = c.PreparationTime;
                            cToAdd.BackgroundColor = c.BackgroundColor;

                            u.Clients.Add(cToAdd);
                            m.SaveChanges();
                            c.IsSuccess = IsSuccess.Yes;
                        }
                        else
                        {
                            var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                            if (cc != null)
                            {
                                cc.Company = c.Company;
                                cc.CompanyEmail = c.CompanyEmail;
                                cc.CompanyPhone = c.CompanyPhone;
                                cc.CompanyAddress = c.CompanyAddress;
                                cc.DeliveryNote = c.DeliveryNote;
                                cc.IsActive = c.IsActive;

                                cc.Domain = c.Domain;
                                cc.HeaderColor = c.HeaderColor;
                                cc.IncludeCompanyEmail = c.IncludeCompanyEmail;
                                cc.IncludeCompanyName = c.IncludeCompanyName;
                                cc.IncludeCompanyPhone = c.IncludeCompanyPhone; ;
                                if(c.LogoImage!=null && c.LogoImage.Length>0)
                                cc.LogoImageID = new BlobManager().AddEditBlob(c.LogoImageID, c.LogoImage);
                                cc.PreparationTime = c.PreparationTime;
                                cc.BackgroundColor = c.BackgroundColor;



                                m.SaveChanges();
                                c.IsSuccess = IsSuccess.Yes;
                            }
                            else
                            {
                                WriteExcaptionMessage(c, "Can Not Find Company.");
                                return c;
                            }
                        }
                       

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }
    }
}
