﻿using DAL;
using Facad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Manager
{
    public class LoginManager : BaseManager
    {

        public Facad.DataContract.User LoginUserWithCode(Facad.DataContract.User user, string code)
        {


            if (string.IsNullOrEmpty(user.UserPassword))
            {
                WriteExcaptionMessage(user, "Password can not be empaty!");
                return user;
            }
            if (string.IsNullOrEmpty(user.UserEmail))
            {
                WriteExcaptionMessage(user, "Email can not be empaty!");
                return user;
            }

            if (string.IsNullOrEmpty(code))
            {
                WriteExcaptionMessage(user, "The Code is not provided!");
                return user;
            }


            user.UserEmail = user.UserEmail.Trim().ToLower();
          



            code = code.Trim();

            using (OOSEntities m = new OOSEntities())
            {
                try
                {


                    var u = m.Users.FirstOrDefault(f => f.UserEmail == user.UserEmail && f.IsActive && !f.IsSuspended);
                    if (u == null)
                    {
                        WriteExcaptionMessage(user, "Can not Find the User!");
                    }
                    else
                    {


                        var secCodes = RegistrationManager.GetFullGuid(u.Guid);

                        var password = SecBroker.HashPassword(user.UserPassword, secCodes.Solt, 5000);
                        
                        var oldCode = secCodes.Guid.Substring(0, Constants.EmailConfirmationCodLenth);


                        if (u.UserPassword == password && oldCode == code)
                        {
                            secCodes = RegistrationManager.GetFullGuid(user.Guid);

                            u.Guid = secCodes.FullGuid;
                            u.UserPassword = SecBroker.HashPassword(user.UserPassword, secCodes.Solt, 5000);
                            u.AtemptsCount = 0;
                            u.LastLogined = DateTime.Now;


                            m.SaveChanges();


                            user.IsSuccess = Facad.DataContract.IsSuccess.Yes;

                            user.Guid = u.Guid;
                        }
                        else
                        {
                            if (u.AtemptsCount > 3)
                            {
                                u.IsSuspended = true;
                            }
                            u.AtemptsCount++;

                            m.SaveChanges();

                            WriteExcaptionMessage(user, "Can not Find the User!");
                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, user);
                }
            }

           

            return user;
        }


        public Facad.DataContract.User LoginUser(Facad.DataContract.User user)
        {


            if (string.IsNullOrEmpty(user.UserPassword))
            {
                WriteExcaptionMessage(user, "Password can not be empaty!");
                return user;
            }
            if (string.IsNullOrEmpty(user.UserEmail))
            {
                WriteExcaptionMessage(user, "Email can not be empaty!");
                return user;
            }

           


            user.UserEmail = user.UserEmail.Trim().ToLower();




       

            using (OOSEntities m = new OOSEntities())
            {
                try
                {


                    var u = m.Users.FirstOrDefault(f => f.UserEmail == user.UserEmail && f.IsActive && !f.IsSuspended);
                    if (u == null)
                    {
                        WriteExcaptionMessage(user, "Can not Find the User!");
                    }
                    else
                    {


                        var secCodes = RegistrationManager.GetFullGuid(u.Guid);

                        var password = SecBroker.HashPassword(user.UserPassword, secCodes.Solt, 5000);

                        var oldCode = secCodes.Guid.Substring(0, Constants.EmailConfirmationCodLenth);


                        if (u.UserPassword == password)
                        {
                            secCodes = RegistrationManager.GetFullGuid(user.Guid);

                            u.Guid = secCodes.FullGuid;
                            u.UserPassword = SecBroker.HashPassword(user.UserPassword, secCodes.Solt, 5000);
                            u.AtemptsCount = 0;
                            u.LastLogined = DateTime.Now;


                            m.SaveChanges();


                            user.IsSuccess = Facad.DataContract.IsSuccess.Yes;

                            user.Guid = u.Guid;
                        }
                        else
                        {
                            if (u.AtemptsCount > 3)
                            {
                                u.IsSuspended = true;
                            }
                            u.AtemptsCount++;

                            m.SaveChanges();

                            WriteExcaptionMessage(user, "Can not Find the User!");
                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, user);
                }
            }



            return user;
        }



        public static object ForgotPasswordSync = new object();
        public Facad.DataContract.User SendForgotPasswordCode(Facad.DataContract.User user)
        {
            if (string.IsNullOrEmpty(user.UserEmail))
            {
                WriteExcaptionMessage(user, "Email can not be empaty!");
                return user;
            }
            user.UserEmail = user.UserEmail.Trim().ToLower();



            lock (ForgotPasswordSync)
            {
                using (OOSEntities m = new OOSEntities())
                {
                    try
                    {


                        var u = m.Users.FirstOrDefault(f => f.UserEmail == user.UserEmail && f.IsActive);
                        if (u == null)
                        {
                            WriteExcaptionMessage(user, "Can not Find the User!");
                        }
                        else
                        {

                            user.UserPassword = SecBroker.GenerateTemproryPassword();
                            var secCodes = RegistrationManager.GetFullGuid();
                            var password = SecBroker.HashPassword(user.UserPassword, secCodes.Solt, 5000);

                            var forgotPassword = u.ForgotPasswords.FirstOrDefault();

                            if (forgotPassword == null)
                            {
                                forgotPassword = new ForgotPassword();
                                u.ForgotPasswords.Add(forgotPassword);
                            }

                            forgotPassword.CreatedDate = DateTime.Now;
                            forgotPassword.Guid = secCodes.FullGuid;
                            forgotPassword.Password = password;
                           
                            m.SaveChanges();
                            user.IsSuccess = Facad.DataContract.IsSuccess.Yes;

                        }
                    }
                    catch (Exception ex)
                    {

                        this.HandleException(ex, user);
                        WriteExcaptionMessage(user, ex.Message);
                    }
                }

            }

            return user;
        }




        public Facad.DataContract.User ResetPasswordFromForgotPassword(Facad.DataContract.User user)
        {

            if (string.IsNullOrEmpty(user.Guid))
            {
                WriteExcaptionMessage(user, "Current Password can not be empaty!");
                return user;
            }

            if (string.IsNullOrEmpty(user.UserPassword))
            {
                WriteExcaptionMessage(user, "New Password can not be empaty!");
                return user;
            }
            if (string.IsNullOrEmpty(user.ConfirmPassword))
            {
                WriteExcaptionMessage(user, "Confirm Password can not be empaty!");
                return user;
            }

            if (string.IsNullOrEmpty(user.UserEmail))
            {
                WriteExcaptionMessage(user, "Email can not be empaty!");
                return user;
            }
            if (user.UserPassword != user.ConfirmPassword)
            {
                WriteExcaptionMessage(user, "Password and Confirm Password do not match!");
                return user;
            }


            user.UserEmail = user.UserEmail.Trim().ToLower();






            using (OOSEntities m = new OOSEntities())
            {
                try
                {


                    var u = m.Users.FirstOrDefault(f => f.UserEmail == user.UserEmail && f.IsActive);
                    if (u == null)
                    {
                        WriteExcaptionMessage(user, "Can not Find the User!");
                        return user;
                    }
                    else
                    {
                        var forgotPassword = u.ForgotPasswords.FirstOrDefault();

                        if (forgotPassword == null)
                        {
                            WriteExcaptionMessage(user, "Can not Find the Forgot Password request for the User!");
                            return user;
                        }
                        else
                        {
                            var secCodes = RegistrationManager.GetFullGuid(forgotPassword.Guid);
                            var password = SecBroker.HashPassword(user.Guid, secCodes.Solt, 5000);

                            if (forgotPassword.Password == password)
                            {
                                secCodes = RegistrationManager.GetFullGuid();

                                u.Guid = secCodes.FullGuid;
                                u.UserPassword = SecBroker.HashPassword(user.UserPassword, secCodes.Solt, 5000);
                                u.AtemptsCount = 0;
                                u.LastLogined = DateTime.Now;
                                u.IsSuspended = false;

                                m.SaveChanges();


                                user.IsSuccess = Facad.DataContract.IsSuccess.Yes;

                                user.Guid = u.Guid;
                            }
                            else
                            {
                                if (u.AtemptsCount > 3)
                                {
                                    u.IsSuspended = true;
                                }
                                u.AtemptsCount++;

                                m.SaveChanges();

                                WriteExcaptionMessage(user, "Incorrect credentials");
                            }

                        }

                     



                       
                        

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, user);
                }
            }



            return user;
        }
    }
}
