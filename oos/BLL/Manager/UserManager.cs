﻿using DAL;
using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Manager
{

    public class UserManager : BaseManager
    {
        public Facad.DataContract.User GetUserByGuid(string userID)
        {

            Facad.DataContract.User user = new Facad.DataContract.User();
            if (string.IsNullOrEmpty(userID))
            {
                WriteExcaptionMessage(user, "Unknown User.");
                return user;
            }

            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(user, "Can Not Find User.");
                        return user;
                    }
                    else
                    {

                        user.Guid = userID;
                        user.Phone = u.Phone;
                        user.UserEmail = u.UserEmail;
                        user.IsSuccess = Facad.DataContract.IsSuccess.Yes;
                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, user);
                }
            }

            return user;
        }


        public Facad.DataContract.User SaveUserPthone(string userID, string phone)
        {

            Facad.DataContract.User user = new Facad.DataContract.User();
            if (string.IsNullOrEmpty(userID))
            {
                WriteExcaptionMessage(user, "Unknown User.");
                return user;
            }
           

          

            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(user, "Can Not Find User.");
                        return user;
                    }
                    else
                    {
                        u.Phone = phone;
                        m.SaveChanges();
                        user.IsSuccess = Facad.DataContract.IsSuccess.Yes;
                     }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, user);
                }
            }

            return user;
        }

        public Facad.DataContract.User SaveUserPassword(string userID, ChangePasswordViewModel model)
        {

            Facad.DataContract.User user = new Facad.DataContract.User();

            if (string.IsNullOrEmpty(userID))
            {
                WriteExcaptionMessage(user, "Unknown User.");
                return user;
            }

            if (string.IsNullOrEmpty(model.OldPassword))
            {
                WriteExcaptionMessage(user, "Current Password can not be empaty!");
                return user;
            }

            if (string.IsNullOrEmpty(model.NewPassword))
            {
                WriteExcaptionMessage(user, "New Password can not be empaty!");
                return user;
            }
            if (string.IsNullOrEmpty(model.ConfirmPassword))
            {
                WriteExcaptionMessage(user, "Confirm Password can not be empaty!");
                return user;
            }

          
            if (user.UserPassword != user.ConfirmPassword)
            {
                WriteExcaptionMessage(user, "Password and Confirm Password do not match!");
                return user;
            }


    





            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(user, "Can not Find the User!");
                        return user;
                    }
                    else
                    {
                        var secCodes = RegistrationManager.GetFullGuid(userID);
                        var  dbPassword = u.UserPassword;
                        var oldPassword = SecBroker.HashPassword(model.OldPassword, secCodes.Solt, 5000);

                        if (dbPassword == oldPassword)
                           {
                                secCodes = RegistrationManager.GetFullGuid();

                                u.Guid = secCodes.FullGuid;
                                u.UserPassword = SecBroker.HashPassword(model.NewPassword, secCodes.Solt, 5000);
                                u.AtemptsCount = 0;
                                u.LastLogined = DateTime.Now;


                                m.SaveChanges();


                                user.IsSuccess = Facad.DataContract.IsSuccess.Yes;

                                user.Guid = u.Guid;
                            user.UserEmail = u.UserEmail;
                        }
                            else
                            {
                                if (u.AtemptsCount > 3)
                                {
                                    u.IsSuspended = true;
                                }
                                u.AtemptsCount++;

                                m.SaveChanges();

                                WriteExcaptionMessage(user, "Incorrect credentials");
                            }

                        }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, user);
                }
            }



            return user;


        }




      


    }
}
