﻿using BLL.InternalModel;
using DAL;
using Facad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Manager
{
    public class RegistrationManager: BaseManager
    {
        private static object SyncObj = new object();
        public Facad.DataContract.User RegistterUser(Facad.DataContract.User user)
        {

            if (string.IsNullOrEmpty(user.ConfirmPassword))
            {
                WriteExcaptionMessage(user, "Confirm Password can not be empaty!");
                return user;
            }
            if (string.IsNullOrEmpty(user.UserPassword))
            { 
                WriteExcaptionMessage(user, "Password can not be empaty!");
                return user;
            }
            if (string.IsNullOrEmpty(user.UserEmail))
            { 
                WriteExcaptionMessage(user, "Email can not be empaty!");
                return user;
            }
            if (user.UserPassword != user.ConfirmPassword)
            { 
                WriteExcaptionMessage(user, "Password and Confirm Password do not match!");
                return user;
            }

            user.UserEmail = user.UserEmail.Trim().ToLower();

            lock (SyncObj)
            {
                using (OOSEntities m = new OOSEntities())
                {
                    try
                    {


                        var u = m.Users.FirstOrDefault(f => f.UserEmail == user.UserEmail);
                        if (u == null)
                        {






                           var secCodes = RegistrationManager.GetFullGuid();





                            u = new User();
                            u.CreatedDate = DateTime.Now;
                          


                            u.Guid = secCodes.FullGuid;
                            u.IsActive = true;
                            u.IsSuspended = false;
                            u.UserEmail = user.UserEmail;
                            u.UserPassword = SecBroker.HashPassword(user.UserPassword, secCodes.Solt, 5000);
                            u.AtemptsCount = 0;
                            u.LastLogined = DateTime.Now;


                            m.Users.Add(u);
                            m.SaveChanges();

                            user.IsSuccess = Facad.DataContract.IsSuccess.Yes;

                            user.Guid = u.Guid;
                        }
                        else
                        {
                            user.IsSuccess = Facad.DataContract.IsSuccess.No;
                            user.Message = "The User with this email is already exist!";
                        }
                    }
                    catch (Exception ex)
                    {

                        this.HandleException(ex, user);
                    }
                }

            }

            return user;
        }



        public static SecurityModel GetFullGuid(string fullGuid=null)
        {
            SecurityModel retVal = new SecurityModel();

            if (string.IsNullOrEmpty(fullGuid))
            {
                var guid = System.Guid.NewGuid().ToString("N");
                retVal.Solt = SecBroker.GenerateSalt();
                var soltString = System.Text.Encoding.Default.GetString(retVal.Solt);
                retVal.FullGuid = guid.Insert(Facad.Constants.EmailConfirmationCodLenth, soltString);
                retVal.Guid = guid;
            }
            else
            {
                var soltString = fullGuid.Substring(Constants.EmailConfirmationCodLenth, Constants.SoltLength);
                retVal.Solt = System.Text.Encoding.Default.GetBytes(soltString);
                retVal.FullGuid = fullGuid;
                //Guid char count 32 - 6 Constants.EmailConfirmationCodLenth) = 26
                int posFrom = fullGuid.Length - 26;

                retVal.Guid = fullGuid.Replace(soltString, "");
            }

           
           


            return retVal;
        }
    }
}
