﻿using DAL;
using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Manager
{
    public class CampaignManager: BaseManager
    {
        public Facad.DataContract.Client GetShipmentsForClient(Facad.DataContract.Client c)
        {
            c.Shipments = new List<Facad.DataContract.Shipment>();

            string userID = c.UserID;
            int clientid = c.ClientID;

            if (string.IsNullOrEmpty(userID))
            {
                WriteExcaptionMessage(c, "Unknown User.");
                return c;
            }

            if (c.ClientID<=0)
            {
                WriteExcaptionMessage(c, "Company Name Can not be empaty.");
                return c;
            }

           

            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                        if (cc == null)
                        {
                            WriteExcaptionMessage(c, "Can Not Find Company.");
                            return c;
                        }
                        else
                        {



                            var ccc = cc.Shipments.AsEnumerable().Select(s => new Facad.DataContract.Shipment
                            {
                               
                                Name = s.Name,
                                ClientID = s.ClientID,
                                CreatedDate = s.CreatedDate,
                               // Description = s.Description,
                                Price = s.Price,
                                ShipmentID = s.ShipmentID,
                                UofM = s.UofM,

                            });
                            c.Company = cc.Company;
                            c.ClientID = clientid;
                            c.Shipments = ccc.ToList();
                            c.IsSuccess = IsSuccess.Yes;
                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }





        public Facad.DataContract.Client GetShipmentForClient(Facad.DataContract.Client c, int ShipmentID)
        {
            c.Shipment = new Facad.DataContract.Shipment();

            string userID = c.UserID;
            int clientid = c.ClientID;

           

            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                        if (cc == null)
                        {
                            WriteExcaptionMessage(c, "Can Not Find User.");
                            return c;
                        }
                        else
                        {


                           
                                var s = cc.Shipments.FirstOrDefault(f => f.ShipmentID == ShipmentID);
                                if (s != null)
                                {
                                    var Shipment = new Facad.DataContract.Shipment();
                                    Shipment.Name = s.Name;
                                    Shipment.ClientID = s.ClientID;
                                    Shipment.CreatedDate = s.CreatedDate;
                                    Shipment.Description = s.Description;
                                    Shipment.GroupOfShipmentID = s.GroupOfShipmentID;
                                    Shipment.Price = s.Price;
                                    Shipment.ShipmentID = s.ShipmentID;
                                    Shipment.UofM = s.UofM;
                                Shipment.ImageID = s.ImageID.GetValueOrDefault(0);
                                c.Shipment = Shipment;

                              //  Shipment.Groups = cc.GroupOfShipments.ToDictionary(g=>g.GroupOfShipmentID, g=>g.GroupName);

                                }
                          
                          
                            c.IsSuccess = IsSuccess.Yes;
                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }


        public Facad.DataContract.Client SaveShipmentForClient(Facad.DataContract.Client c, int ShipmentID)
        {
       
            var dto = c.Shipment;

          

            string userID = c.UserID;
            int clientid = c.ClientID;
           

            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                        if (cc == null)
                        {
                            WriteExcaptionMessage(c, "Can Not Find User.");
                            return c;
                        }
                        else
                        {


                            if (ShipmentID > 0)
                            {
                                var s = cc.Shipments.FirstOrDefault(f => f.ShipmentID == ShipmentID);
                                if (s != null)
                                {
                                    s.CreatedDate = DateTime.Now;
                                    s.Description = dto.Description;
                                    s.GroupOfShipmentID = dto.GroupOfShipmentID;
                                    s.Name = dto.Name;
                                    s.Price = dto.Price;
                                    s.UofM = dto.UofM;
                                    if(dto.Image!=null && dto.Image.Length>0)
                                    s.ImageID = new BlobManager().AddEditBlob(dto.ImageID, dto.Image);
                                    m.SaveChanges();
                                    c.IsSuccess = IsSuccess.Yes;
                                }
                                else
                                {
                                    WriteExcaptionMessage(c, "Can Not Find Shipment.");
                                    return c;
                                }
                            }
                            else
                            {
                                DAL.Shipment shipment = new DAL.Shipment();
                                shipment.ClientID = clientid;
                                shipment.CreatedDate = DateTime.Now;
                                shipment.Description = dto.Description;
                                shipment.GroupOfShipmentID = dto.GroupOfShipmentID;
                                shipment.Name = dto.Name;
                                shipment.Price = dto.Price;
                                if (dto.Image != null && dto.Image.Length > 0)
                                    shipment.ImageID = new BlobManager().AddEditBlob(dto.ImageID, dto.Image);
                                shipment.UofM = dto.UofM;
                                cc.Shipments.Add(shipment);
                                m.SaveChanges();
                                c.IsSuccess = IsSuccess.Yes;
                            }

                           
                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }


        public Facad.DataContract.Client DeleteShipmentForClient(Facad.DataContract.Client c, int ShipmentID)
        {

            var dto = c.Shipment;



            string userID = c.UserID;
            int clientid = c.ClientID;


            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                        if (cc == null)
                        {
                            WriteExcaptionMessage(c, "Can Not Find User.");
                            return c;
                        }
                        else
                        {


                            if (ShipmentID > 0)
                            {
                                var s = cc.Shipments.FirstOrDefault(f => f.ShipmentID == ShipmentID);
                                if (s != null)
                                {
                                    m.Shipments.Remove(s);
                                    m.SaveChanges();
                                    c.IsSuccess = IsSuccess.Yes;
                                }
                                else
                                {
                                    WriteExcaptionMessage(c, "Can Not Find Shipment.");
                                    return c;
                                }
                            }
                            else
                            {
                                WriteExcaptionMessage(c, "Can Not Find Shipment.");
                                return c;
                            }


                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }
    }
}
