﻿using DAL;
using System;
using Facad.DataContract;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Manager
{
    public class GroupManager: BaseManager
    {

        public Facad.DataContract.Client GetGroupsForClient(Facad.DataContract.Client c)
        {
            c.Shipments = new List<Facad.DataContract.Shipment>();

            string userID = c.UserID;
            int clientid = c.ClientID;

            if (string.IsNullOrEmpty(userID))
            {
                WriteExcaptionMessage(c, "Unknown User.");
                return c;
            }

            if (c.ClientID <= 0)
            {
                WriteExcaptionMessage(c, "Company Name Can not be empaty.");
                return c;
            }



            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                        if (cc == null)
                        {
                            WriteExcaptionMessage(c, "Can Not Find Company.");
                            return c;
                        }
                        else
                        {
                            c.Groups = cc.GroupOfShipments.Where(w=>w.IsActive).ToDictionary(g => g.GroupOfShipmentID, g => g.GroupName);
                            c.Company = cc.Company;
                            c.CompanyEmail = cc.CompanyEmail;
                            c.CompanyPhone = cc.CompanyPhone;
                            c.DeliveryNote = cc.DeliveryNote;
                            c.CompanyAddress = cc.CompanyAddress;

                            c.IsSuccess = IsSuccess.Yes;
                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }



        public Facad.DataContract.Client GetGroupForClient(Facad.DataContract.Client c)
        {

            string userID = c.UserID;
            int clientid = c.ClientID;



            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                        if (cc == null)
                        {
                            WriteExcaptionMessage(c, "Can Not Find User.");
                            return c;
                        }
                        else
                        {



                            var s = cc.GroupOfShipments.FirstOrDefault(f => f.GroupOfShipmentID == c.GroupID);
                            if (s != null)
                            {


                                c.GroupName = s.GroupName;
                                c.GroupID = s.GroupOfShipmentID;

                            }


                            c.IsSuccess = IsSuccess.Yes;
                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }


        public Facad.DataContract.Client SaveGroupForClient(Facad.DataContract.Client c)
        {

     



            string userID = c.UserID;
            int clientid = c.ClientID;


            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                        if (cc == null)
                        {
                            WriteExcaptionMessage(c, "Can Not Find User.");
                            return c;
                        }
                        else
                        {


                            if (c.GroupID > 0)
                            {
                                var s = cc.GroupOfShipments.FirstOrDefault(f => f.GroupOfShipmentID == c.GroupID);
                                if (s != null)
                                {
                                    s.GroupName = c.GroupName;
                                    
                                    m.SaveChanges();
                                    c.IsSuccess = IsSuccess.Yes;
                                }
                                else
                                {
                                    WriteExcaptionMessage(c, "Can Not Find Shipment.");
                                    return c;
                                }
                            }
                            else
                            {
                                var newGroup = new GroupOfShipment { GroupName = c.GroupName, IsActive = true };
                                cc.GroupOfShipments.Add(newGroup);
                              
                                m.SaveChanges();
                                c.GroupID = newGroup.GroupOfShipmentID;
                                c.IsSuccess = IsSuccess.Yes;
                            }


                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }


        public Facad.DataContract.Client DeleteGroupForClient(Facad.DataContract.Client c, int GroupID)
        {

            var dto = c.Shipment;



            string userID = c.UserID;
            int clientid = c.ClientID;


            using (OOSEntities m = new OOSEntities())
            {
                try
                {
                    var u = m.Users.FirstOrDefault(f => f.Guid == userID);
                    if (u == null)
                    {
                        WriteExcaptionMessage(c, "Can Not Find User.");
                        return c;
                    }
                    else
                    {

                        var cc = u.Clients.FirstOrDefault(f => f.ClientID == clientid);
                        if (cc == null)
                        {
                            WriteExcaptionMessage(c, "Can Not Find User.");
                            return c;
                        }
                        else
                        {


                            if (GroupID > 0)
                            {
                                var s = cc.GroupOfShipments.FirstOrDefault(f => f.GroupOfShipmentID == GroupID);
                                if (s != null)
                                {
                                    s.IsActive = false;
                                    m.SaveChanges();
                                    c.IsSuccess = IsSuccess.Yes;
                                }
                                else
                                {
                                    WriteExcaptionMessage(c, "Can Not Find Shipment.");
                                    return c;
                                }
                            }
                            else
                            {
                                WriteExcaptionMessage(c, "Can Not Find Shipment.");
                                return c;
                            }


                        }

                    }
                }
                catch (Exception ex)
                {

                    this.HandleException(ex, c);
                }
            }

            return c;
        }
    }
}
