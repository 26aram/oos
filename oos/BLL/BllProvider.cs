﻿using Facad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facad.DataContract;
using BLL.Manager;

namespace BLL
{
    public class BllProvider : IBll
    {
        #region Authentication Autorization

     
        public Task<User> RegistterUser(User user)
        {
            using (RegistrationManager m = new RegistrationManager())
            {
                return Task.Run(()=>m.RegistterUser(user));
            }
        }

        public Task<User> LoginUserWithCode(User user, string code)
        {
            using (LoginManager m = new LoginManager())
            {
                return Task.Run(() => m.LoginUserWithCode(user, code));
            }
        }

        public Task<User> LoginUser(User user)
        {
            using (LoginManager m = new LoginManager())
            {
                return Task.Run(() => m.LoginUser(user));
            }
        }
        public Task<User> SendForgotPasswordCode(Facad.DataContract.User user)
        {
            using (LoginManager m = new LoginManager())
            {
                return Task.Run(() => m.SendForgotPasswordCode(user));
            }
        }
        public Task<User> ResetPasswordFromForgotPassword(Facad.DataContract.User user)
        {
            using (LoginManager m = new LoginManager())
            {
                return Task.Run(() => m.ResetPasswordFromForgotPassword(user));
            }
        }


        public Task<User> GetUserByGuid(string userID)
        {
            using (UserManager m = new UserManager())
            {
                return Task.Run(() => m.GetUserByGuid(userID));
            }
        }

        public Task<User> SaveUserPthone(string userID, string phone)
        {
            using (UserManager m = new UserManager())
            {
                return Task.Run(() => m.SaveUserPthone(userID, phone));
            }
        }
        public Task<User> SaveUserPassword(string userID, ChangePasswordViewModel model)
        {
            using (UserManager m = new UserManager())
            {
                return Task.Run(() => m.SaveUserPassword(userID, model));
            }
        }
        #endregion


        #region Client
        public Task<Clients> GetClientsForUser(Clients c)
        {
            using (ClientManager m = new ClientManager())
            {
                return Task.Run(() => m.GetClientsForUser(c));
            }
        }

        public Task<Client> GetClientForUser(Client c)
        {
            using (ClientManager m = new ClientManager())
            {
                return Task.Run(() => m.GetClientForUser(c));
            }
        }
        public Task<Client> SaveClientForUser(Client c)
        {
            using (ClientManager m = new ClientManager())
            {
                return Task.Run(() => m.SaveClientForUser(c));
            }
        }
       

        #endregion
        #region Blob

        public Task<byte[]> GetFile(int blobID)
        {
            using (BlobManager m = new BlobManager())
            {
                return Task.Run(() => m.GetFile(blobID));
            }
        }

        #endregion


        #region Shipment
        public Task<Client> GetShipmentsForClient(Client c)
        {
            using (CampaignManager m = new CampaignManager())
            {
                return Task.Run(() => m.GetShipmentsForClient(c));
            }
        }

        public Task<Client> GetShipmentForClient(Client c, int shipmentID)
        {
            using (CampaignManager m = new CampaignManager())
            {
                return Task.Run(() => m.GetShipmentForClient(c, shipmentID));
            }
        }
        public Task<Client> SaveShipmentForClient(Client c, int shipmentID)
        {
            using (CampaignManager m = new CampaignManager())
            {
                return Task.Run(() => m.SaveShipmentForClient(c, shipmentID));
            }
        }

        public Task<Client> DeleteShipmentForClient(Client c, int shipmentID)
        {
            using (CampaignManager m = new CampaignManager())
            {
                return Task.Run(() => m.DeleteShipmentForClient(c, shipmentID));
            }
        }
        #endregion

        #region Groups
        public Task<Client> GetGroupsForClient(Client c)
        {
            using (GroupManager m = new GroupManager())
            {
                return Task.Run(() => m.GetGroupsForClient(c));
            }
        }

        public Task<Client> GetGroupForClient(Client c)
        {
            using (GroupManager m = new GroupManager())
            {
                return Task.Run(() => m.GetGroupForClient(c));
            }
        }

        public Task<Client> SaveGroupForClient(Client c)
        {
            using (GroupManager m = new GroupManager())
            {
                return Task.Run(() => m.SaveGroupForClient(c));
            }
        }
        public Task<Client> DeleteGroupForClient(Client c, int GroupID)
        {
            using (GroupManager m = new GroupManager())
            {
                return Task.Run(() => m.DeleteGroupForClient(c, GroupID));
            }
        }
        #endregion

    }
}
