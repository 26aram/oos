﻿using Facad;
using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace EmailService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class EmailService : IEmailService
    {
        public const EmailSendProvider provider = EmailSendProvider.Gmail;


        private BaseManager GetCurrentProvider()
        {
            switch (provider)
            {
                case EmailSendProvider.LocalSmtp:
                    return new SmtpManager();

                case EmailSendProvider.Gmail:
                    return new GmailManager();
                default:
                    return null;

            }
        }

        public Task<string> Send_EmailConfirmation_Email(List<string> to, string code)
        {
            var provider = GetCurrentProvider();

            return Task.Run(() => provider.Send(to, MessageTypeEnum.ComfirmEmail, code));

        }
        public Task<string> Send_TemporaryPassword_Email(List<string> to, string code)
        {
            var provider = GetCurrentProvider();

            return Task.Run(() => provider.Send(to, MessageTypeEnum.SendTemporaryPassword, code));
           
        }


        public Task<string> Send_PaswordChangedConfirmation_Email(List<string> to, string code)
        {
            var provider = GetCurrentProvider();

            return Task.Run(() => provider.Send(to, MessageTypeEnum.PasswordCangedConfirmation, code));

        }
    }
}
