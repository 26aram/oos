﻿using Facad.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;

namespace EmailService
{
    public abstract class BaseManager
    {
        public const string link = "http://localhost:35943";
        public abstract string Send(List<string> to, MessageTypeEnum type, string code);

        protected MailMessage GetMessage(MailAddress from, List<string> to, MessageTypeEnum type, string code)
        {
            MailMessage mailMsg = new MailMessage();

            // To
            foreach (var item in to)
            {
                mailMsg.To.Add(item);
            }


            // From
            mailMsg.From = from;
            StringBuilder text = new StringBuilder();
            StringBuilder html = new StringBuilder();
            switch (type)
            {
                case MessageTypeEnum.ComfirmEmail:
                    mailMsg.Subject = "Welcome to Macazon.com";
                  
                    text.AppendLine("Hi,");
                    text.AppendLine("Thank You for joining Macazon.com");
                    text.AppendLine();
                    text.AppendFormat("Please click the link below to login with the folowing code:{0}", code);
                    text.AppendLine();
                    text.AppendFormat("{0}/account/confirm", link);


                    html.AppendLine("<h2>Hi,</h2>");
                    html.AppendLine("<h1>Thank You for joining Macazon.com</h1>");
                    html.AppendFormat("<p>Please click the link below to login with the folowing code:{0}</p>", code);
                    html.AppendLine("<br/>");
                    html.AppendFormat("<p>{0}/account/confirm</p>", link);


                 
                    mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text.ToString(), null, MediaTypeNames.Text.Plain));
                    mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html.ToString(), null, MediaTypeNames.Text.Html));
                    break;

                case MessageTypeEnum.SendTemporaryPassword:
                    mailMsg.Subject = "Temporary Password";
                   
                    text.AppendLine("Hi,");
                    text.AppendLine("Thank You for using Macazon.com");
                    text.AppendLine();
                    text.AppendLine("You have requested Forgot Password.");
                    text.AppendFormat("Please click the link below to change password with the folowing Temporary Password:{0}", code);
                    text.AppendLine();
                    text.AppendFormat("{0}/Account/ResetPassword", link);
                    text.AppendLine();
                    text.AppendLine("And set new password.");
                    text.AppendLine();
                    text.AppendLine("If you did not request to change password, just ignor this message!");


                    html.AppendLine("<h2>Hi,</h2>");
                    html.AppendLine("<h1>Thank You for using Macazon.com</h1>");
                    html.AppendLine("<p>You have requested Forgot Password.</p>");
                    html.AppendFormat("<p>Please click the link below to change password with the folowing Temporary Password:{0}</p>", code);
                    html.AppendLine("<br/>");
                    html.AppendFormat("<p>{0}/Account/ResetPassword</p>", link);
                    html.AppendLine("<br/>");
                    html.AppendLine("<p>And set new password.</p>");
                    html.AppendLine("<br/>");
                    html.AppendLine("<p>If you did not request to change password, just ignor this message!</p>");




                    mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text.ToString(), null, MediaTypeNames.Text.Plain));
                    mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html.ToString(), null, MediaTypeNames.Text.Html));
                    break;

                case MessageTypeEnum.PasswordCangedConfirmation:
                    mailMsg.Subject = "Password Changed confirmation";
                  
                    text.AppendLine("Hi,");
                    text.AppendLine("Thank You for using Macazon.com");
                    text.AppendLine();
                    text.AppendLine("Password changed successfully!");
                    text.AppendLine();
                  


                    html.AppendLine("<h2>Hi,</h2>");
                    html.AppendLine("<h1>Thank You for using Macazon.com</h1>");
                    html.AppendLine("<p>Password changed successfully!</p>");
                    html.AppendLine("<br/>");
                 

                 
                    mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text.ToString(), null, MediaTypeNames.Text.Plain));
                    mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html.ToString(), null, MediaTypeNames.Text.Html));
                    break;

                default:
                    break;
            }
          



            return mailMsg;
        }




    }
}