﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using Facad.DataContract;

namespace EmailService
{
    public class GmailManager: BaseManager
    {
        const string fromPassword = "26Mailinator";
        const string fromAddress = "macazon.com@gmail.com";
        public override string Send(List<string> to, MessageTypeEnum type, string code)
        {
            string msg = "OK";
            try
            {

                var mailMsg = this.GetMessage(new MailAddress(fromAddress), to, type, code);
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress, fromPassword)
                };

                smtp.Send(mailMsg);

            }
            catch (Exception exp)
            {

                return exp.Message;
            }

            return msg;
        }

       
       
    }
}