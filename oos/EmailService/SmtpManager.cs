﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net.Mime;
using Facad.DataContract;

namespace EmailService
{
    public class SmtpManager: BaseManager
    {
        const string fromPassword = "yourpassword";
        const string fromAddress = "username@domain.com";
        public override string Send(List<string> to, MessageTypeEnum type, string code)
        {
            string msg = "OK";
            try
            {
                var mailMsg = this.GetMessage(new MailAddress(fromAddress), to, MessageTypeEnum.ComfirmEmail, code);
                // Init SmtpClient and send
                SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(fromAddress, fromPassword);
                smtpClient.Credentials = credentials;

                smtpClient.Send(mailMsg);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return msg;
        }

     
    }
}