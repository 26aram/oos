﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class SecBroker
    {

        public const int SoltLength = 32;
        public const int TemproryPasswordLength = 6;
        //http://immortalcoder.blogspot.com/2015/11/best-way-to-secure-password-using-cryptographic-algorithms-in-csharp-dotnet.html
        public static string HashPassword(string passwordToHash, byte[] solt, int numberOfRounds)
        {
          
            var hashedPassword = HashPassword(Encoding.UTF8.GetBytes(passwordToHash), solt, numberOfRounds);


            //Console.WriteLine("Password to hash : " + passwordToHash);
            //Console.WriteLine("PBKDF2 Hashed Password : " + Convert.ToBase64String(hashedPassword));
            //Console.WriteLine("Iterations : " + numberOfRounds);
            //Console.WriteLine("Elapsed Time : " + sw.ElapsedMilliseconds + "ms");
            //Console.WriteLine();

          
            return Convert.ToBase64String(hashedPassword);
        }


        public static byte[] GenerateSalt()
        {
            using (var randomNumberGenerator = new RNGCryptoServiceProvider())
            {
                var randomNumber = new byte[SoltLength];
                randomNumberGenerator.GetBytes(randomNumber);

                return randomNumber;
            }
        }

        private static byte[] HashPassword(byte[] toBeHashed, byte[] salt, int numberOfRounds)
        {
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(toBeHashed, salt, numberOfRounds))
            {
                return rfc2898DeriveBytes.GetBytes(32);
            }
        }
        public static string GenerateTemproryPassword()
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];
                int length = TemproryPasswordLength;
                while (length-- > 0)
                {
                    rng.GetBytes(uintBuffer);
                    uint num = BitConverter.ToUInt32(uintBuffer, 0);
                    res.Append(valid[(int)(num % (uint)valid.Length)]);
                }
            }

            return res.ToString();
        }

    }
}
